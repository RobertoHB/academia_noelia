<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cuentas_bancarias".
 *
 * @property int $id
 * @property int $alumno
 * @property string $iban
 * @property int $entidad
 * @property int $oficina
 * @property int $dc
 * @property int $cuenta
 *
 * @property Alumnos $alumno0
 */
class Cuentas_bancarias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cuentas_bancarias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alumno', 'entidad', 'oficina', 'dc', 'cuenta'], 'integer'],
            [['iban'], 'string', 'max' => 4],
            [['alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['alumno' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alumno' => 'Alumno',
            'iban' => 'Iban',
            'entidad' => 'Entidad',
            'oficina' => 'Oficina',
            'dc' => 'Dc',
            'cuenta' => 'Cuenta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlumno0()
    {
        return $this->hasOne(Alumnos::className(), ['id' => 'alumno']);
    }
}
