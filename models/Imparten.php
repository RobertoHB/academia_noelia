<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "imparten".
 *
 * @property int $id
 * @property int $profesor
 * @property int $asignatura
 *
 * @property Asignaturas $asignatura0
 * @property Profesores $profesor0
 */
class Imparten extends \yii\db\ActiveRecord
{
       public $codigoProfesor;
       public $nombreProfesor;
       public $apellidosProfesor;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'imparten';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profesor', 'asignatura'], 'integer'],
            [['profesor', 'asignatura'], 'unique', 'targetAttribute' => ['profesor', 'asignatura']],
            [['asignatura'], 'exist', 'skipOnError' => true, 'targetClass' => Asignaturas::className(), 'targetAttribute' => ['asignatura' => 'id']],
            [['profesor'], 'exist', 'skipOnError' => true, 'targetClass' => Profesores::className(), 'targetAttribute' => ['profesor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profesor' => 'Profesor',
            'asignatura' => 'Asignatura',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsignatura0()
    {
        return $this->hasOne(Asignaturas::className(), ['id' => 'asignatura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesor0()
    {
        return $this->hasOne(Profesores::className(), ['id' => 'profesor']);
    }
    
     public function setIdProfesor($codigo){
         $this->codigoProfesor = $codigo;
        
    }
     public function setNombreProfesor($nombre){
         $this->nombreProfesor = $nombre;
        
    }
    public function setApellidosProfesor($apellidos){
         $this->apellidosProfesor = $apellidos;
        
    }
    public function getCodigoProfesor(){
        return $this->codigoProfesor;
    }
    public function getNombreProfesor(){
        return $this->nombreProfesor;
    }
    public function getApellidosProfesor(){
        return $this->apellidosProfesor;
    }
}
