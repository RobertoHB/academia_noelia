<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "centros".
 *
 * @property int $id
 * @property string $nombre
 * @property string $localidad
 */
class Centros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'centros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 200],
            [['localidad'], 'string', 'max' => 100],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'localidad' => 'Localidad',
        ];
    }
}
