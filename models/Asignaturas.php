<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asignaturas".
 *
 * @property int $id
 * @property string $asignatura
 *
 * @property Clases[] $clases
 * @property Cursos[] $cursos
 * @property Imparten[] $impartens
 * @property Profesores[] $profesors
 */
class Asignaturas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asignaturas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asignatura'], 'string', 'max' => 50],
            [['asignatura'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'asignatura' => 'Asignatura',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClases()
    {
        return $this->hasMany(Clases::className(), ['asignatura' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCursos()
    {
        return $this->hasMany(Cursos::className(), ['id' => 'curso'])->viaTable('clases', ['asignatura' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImpartens()
    {
        return $this->hasMany(Imparten::className(), ['asignatura' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesors()
    {
        return $this->hasMany(Profesores::className(), ['id' => 'profesor'])->viaTable('imparten', ['asignatura' => 'id']);
    }
}
