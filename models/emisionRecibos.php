<?php

namespace app\models;


use Yii;

/**
 * This is the model class for table "recibos".
 *
 * @property int $id
 * @property int $matricula
 * @property string $emision
 * @property int $mes
 * @property int $anyo
 * @property int $estado
 * @property int $reducido
 * @property double $importe
 *
 * @property Matriculas $matricula0
 */

 
 
class EmisionRecibos extends Recibos
{
    public $fechaEmision;
    public $mesRecibo;
    public $anyoRecibo;
    /**
     * {@inheritdoc}
     */
  

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaEmision', 'date'], 'required'],
            [['mesRecibo', 'integer'], 'required'],
            [['anyoRecibo', 'integer'], 'required'],
//            ['fechaEmision','validateEmision'],
           // ['anyoRecibo', 'validateEmitidos'],
               
               
       ];        
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
         $labels = ['fechaEmision' => 'fechaEmision', 'mesRecibo' => 'mesRecibo', 'anyoRecibo' => 'anyoRecibo'];
        return array_merge(parent::attributeLabels(), $labels);
       
    }

     public function validarEmision($mesEmi,$anyoEmi){
        
        $emitidos = Recibos::find()
                    ->where("mes = $mesEmi")
                    ->andWhere("anyo = $anyoEmi");
        
        if($emitidos->count() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    
//    public function validateEmision($attribute,$params){
//        if(Recibos::model()->exists('emision=:fechaEmision',array('fechaEmision'=>$this->fechaEmision))){
//
//            $this->addError('fechaEmision','La fecha de emisión seleccionada ya existe.');
//            
//        }
//    }
    
//    public function validateEmitidos($attribute)
//    {   
//        var_dump($attribute);
//        exit;
//  //Buscar el mes y anyo en la tabla
//  $table = Recibos::find()
//          ->where("mes = $mesRecibo")
//          ->andWhere("anyo = $anyoRecibo");
//  
//  //Si el mes y anyo ya existe mostrar el error
//  if ($table->count() == 1){
//    $this->addError($attribute, "Ya existen los recibos correspondientes a ese mes y anyo.");
//    
//  }
//    }
}
