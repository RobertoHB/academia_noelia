<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clases".
 *
 * @property int $id
 * @property int $curso
 * @property int $asignatura
 * @property double $importe
 * @property string $observaciones
 *
 * @property Asignaturas $asignatura0
 * @property Cursos $curso0
 * @property Matriculas[] $matriculas
 */
class Clases extends \yii\db\ActiveRecord
{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clases';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['curso', 'asignatura'], 'integer'],
            [['importe'], 'number'],
            [['observaciones'], 'string'],
            [['curso', 'asignatura'], 'unique', 'targetAttribute' => ['curso', 'asignatura']],
            [['asignatura'], 'exist', 'skipOnError' => true, 'targetClass' => Asignaturas::className(), 'targetAttribute' => ['asignatura' => 'id']],
            [['curso'], 'exist', 'skipOnError' => true, 'targetClass' => Cursos::className(), 'targetAttribute' => ['curso' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'curso' => 'Curso',
            'asignatura' => 'Asignatura',
            'importe' => 'Importe',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsignatura0()
    {
        return $this->hasOne(Asignaturas::className(), ['id' => 'asignatura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurso0()
    {
        return $this->hasOne(Cursos::className(), ['id' => 'curso']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matriculas::className(), ['clase' => 'id']);
    }
     public function getListaAsignaturas()
    {
        return $this->hasMany(Asignaturas::className(), ['asignatura' => 'id']);
    }
    
}
