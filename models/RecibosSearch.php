<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Recibos;

/**
 * RecibosSearch represents the model behind the search form of `app\models\Recibos`.
 */
class RecibosSearch extends Recibos
{
     public $filtro_alumno;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'matricula', 'mes', 'anyo', 'estado', 'reducido'], 'integer'],
            [['emision','filtro_alumno'], 'safe'],
            [['importe'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = Recibos::find();
        $query = Recibos::find()->joinWith(['alumno0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
         $dataProvider->setSort([
        'attributes' => [
            'id',
            'filtro_alumno', 
            'maticula',
            'emision',
            'mes',
            'anyo',
            'estado',
            'reducido',
            'importe',
            
            ]
        ]);

        
        if (!($this->load($params) && $this->validate())) {
         return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'filtro_alumno' => $this->filtro_alumno,
            'matricula' => $this->matricula,
            'emision' => $this->emision,
            'mes' => $this->mes,
            'anyo' => $this->anyo,
            'estado' => $this->estado,
            'reducido' => $this->reducido,
            'importe' => $this->importe,
        ]);
         
        //$query->andFilterWhere(['like', 'apellidosnombre', $this->filtro_alumno]);
         //->andFilterWhere(['like','vchName',Yii::$app->request->post('vchName')])
        //$query->andWhere('alumno0.apellidos "%' . $this->filtro_alumno.'%"');
        $query->andWhere(['like', 'alumnos.apellidos', $this->filtro_alumno]); 
        
    //);
        return $dataProvider;
    }
}
