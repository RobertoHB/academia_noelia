<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profesores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $direccion
 * @property string $poblacion
 * @property string $dni
 * @property int $movil
 * @property int $fijo
 * @property string $email
 * @property string $alta
 * @property string $baja
 * @property string $observaciones
 *
 * @property Imparten[] $impartens
 * @property Asignaturas[] $asignaturas
 */
class Profesores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profesores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['movil', 'fijo'], 'integer'],
            [['alta', 'baja'], 'safe'],
            [['observaciones'], 'string'],
            [['nombre', 'poblacion'], 'string', 'max' => 100],
            [['apellidos', 'direccion', 'email'], 'string', 'max' => 200],
            [['dni'], 'string', 'max' => 9],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'direccion' => 'Direccion',
            'poblacion' => 'Poblacion',
            'dni' => 'Dni',
            'movil' => 'Movil',
            'fijo' => 'Fijo',
            'email' => 'Email',
            'alta' => 'Alta',
            'baja' => 'Baja',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImpartens()
    {
        return $this->hasMany(Imparten::className(), ['profesor' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsignaturas()
    {
        return $this->hasMany(Asignaturas::className(), ['id' => 'asignatura'])->viaTable('imparten', ['profesor' => 'id']);
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->alta=Yii::$app->formatter->asDate($this->alta, 'php:d-m-Y');
        $this->baja=Yii::$app->formatter->asDate($this->baja, 'php:d-m-Y');

    }

    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          $this->alta=Yii::$app->formatter->asDate($this->alta, 'php:Y-m-d');
          $this->baja=Yii::$app->formatter->asDate($this->baja, 'php:Y-m-d');
          //$this->alta= \DateTime::createFromFormat("d/m/Y", $this->alta)->format("Y/m/d");
          return true;
    }
}
