<?php

namespace app\models;


use Yii;

/**
 * This is the model class for table "recibos".
 *
 * @property int $id
 * @property int $matricula
 * @property string $emision
 * @property int $mes
 * @property int $anyo
 * @property int $estado
 * @property int $reducido
 * @property double $importe
 *
 * @property Matriculas $matricula0
 */

 
 
class ExportacionRecibos extends Recibos
{
    public $mesIni;
    public $mesFin;
    public $anyoRecibo;
    public $alumno;
    public $tipo;
    public $formato;
    /**
     * {@inheritdoc}
     */
  

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mesIni'], 'integer'],
           
            [['mesFin'], 'integer'],
         
            [['anyoRecibo'], 'integer'],
            
            [['alumno'], 'integer'],
          
            [['tipo'],'string', 'max' => 50],   
            [['formato'],'string', 'max' => 50],  
            
             [['mesIni','mesFin','anyoRecibo','tipo'], 'required',
                  'message'=>'El campo no puede estar vacio'],
            
       ];     
        
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
         $labels = ['mesIni' => 'mesIni', 'mesFin' => 'mesFin', 'anyoRecibo' => 'anyoRecibo','alumno' => 'alumno', 'tipo' => 'tipo', 'formato' => 'formato'];
        return array_merge(parent::attributeLabels(), $labels);
    }

   
    
   
}
