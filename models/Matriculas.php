<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "matriculas".
 *
 * @property int $id
 * @property int $alumno
 * @property int $clase
 * @property string $alta
 * @property string $baja
 * @property string $observaciones
 *
 * @property Alumnos $alumno0
 * @property Clases $clase0
 * @property Recibos[] $recibos
 */
class Matriculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matriculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alumno', 'clase'], 'integer'],
            [['alta', 'baja'], 'safe'],
            [['alta','baja'],'default','value'=>''],
            [['observaciones'], 'string'],
            [['alumno', 'clase', 'alta'], 'unique', 'targetAttribute' => ['alumno', 'clase', 'alta']],
            [['alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['alumno' => 'id']],
            [['clase'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['clase' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alumno' => 'Alumno',
            'clase' => 'Clase',
            'alta' => 'Alta',
            'baja' => 'Baja',
            'observaciones' => 'Observaciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlumno0()
    {
        return $this->hasOne(Alumnos::className(), ['id' => 'alumno']);
    }
    
    public function getNombre()
    {
        return $this->hasOne(Alumnos::className(), ['id' => 'nombre'])
         ->via('Alumno');
    }
   
    
    public function getApellidos()
    {
          return $this->hasOne(Alumnos::className(), ['id' => 'apellidos'])
         ->via('Alumno');
        
    }        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClase0()
    {
        return $this->hasOne(Clases::className(), ['id' => 'clase']);
    }
    
    public function getCurso0(){
        
        return $this->hasOne(Cursos::className(), ['id' => 'curso'])
            ->via('clase0');
        
    }
      public function getAsignatura0(){
        
          return $this->hasOne(Asignaturas::className(), ['id' => 'asignatura'])
          ->via('clase0');
    }
    
      public function afterFind() {
        parent::afterFind();
        $this->alta=Yii::$app->formatter->asDate($this->alta, 'php:d-m-Y');
        if($this->baja != Null){
            $this->baja=Yii::$app->formatter->asDate($this->baja, 'php:d-m-Y');
        }else{
            $this->baja = " ";
        }    
    }
    
    

    
    public function beforeSave($insert) {
           if($this->alta != Null){
            $this->alta=Yii::$app->formatter->asDate($this->alta, 'php:Y-m-d');
            if($this->baja != Null){
                 $this->baja=Yii::$app->formatter->asDate($this->baja, 'php:Y-m-d');
            }else{
                $this->baja = Null; 
             }
          }else{
              $this->alta = Null;
             
          }
           return parent::beforeSave($insert);
      
        
    }
    
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecibos()
    {
        return $this->hasMany(Recibos::className(), ['matricula' => 'id']);
    }
}
