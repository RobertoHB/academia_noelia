<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cuentas_bancarias;

/**
 * Cuentas_bancariasSearch represents the model behind the search form of `app\models\Cuentas_bancarias`.
 */
class Cuentas_bancariasSearch extends Cuentas_bancarias
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'alumno', 'entidad', 'oficina', 'dc', 'cuenta'], 'integer'],
            [['iban'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cuentas_bancarias::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'alumno' => $this->alumno,
            'entidad' => $this->entidad,
            'oficina' => $this->oficina,
            'dc' => $this->dc,
            'cuenta' => $this->cuenta,
        ]);

        $query->andFilterWhere(['like', 'iban', $this->iban]);

        return $dataProvider;
    }
}
