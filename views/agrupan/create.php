<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Agrupan */

$this->title = 'Añadir Agrupamiento';
//$this->params['breadcrumbs'][] = ['label' => 'Agrupar', 'url' => ['index','id' => $_GET['id'],'nombre'=>$_GET['nombre'],'apellidos'=>$_GET['apellidos']]];
$this->params['breadcrumbs'][] = ['label' => 'Agrupar/alumno', 'url' =>Yii::$app->request->referrer ?: Yii::$app->homeUrl];

$this->params['breadcrumbs'][] = $this->title;
echo("<div style='float:right'><h3>".$_GET['nombre']." ".$_GET['apellidos']."</h3></div>");
?>
<div class="agrupan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
