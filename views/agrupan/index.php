<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Alumnos;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AgrupanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$codigoAlumno = $_GET['id'];
$nombreAlumno = $_GET['nombre'];
$apellidosAlumno = $_GET['apellidos'];

$this->title = 'Agrupar';
echo("<div style='float:right'><h3>".$nombreAlumno." ".$apellidosAlumno."</h3></div>");
//$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = ['label' => 'alumnos', 'url' => ['/alumnos']];
//$this->params['breadcrumbs'][] = ['label' => 'alumnos', 'url' =>Yii::$app->request->referrer ?: Yii::$app->homeUrl];
?>
<div class="agrupan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir Agrupaciones', ['create','id' => $codigoAlumno,'nombre'=>$nombreAlumno,'apellidos'=>$apellidosAlumno], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
           // 'alumno',
           // 'alumno_grupo',
            [
              'label' => 'Nombre',
              'attribute' => 'alumno_grupo',
               'value' => 'alumnoGrupo0.nombre',
                'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id', 'nombre'),
              'enableSorting' => true,
            ],
             [
              'label' => 'Apellidos',
              'attribute' => 'alumno_grupo',
               'value' => 'alumnoGrupo0.apellidos',
                'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id', 'apellidos'),
              'enableSorting' => true,
            ],
           
           
            'observaciones:ntext',

             ['class' => 'yii\grid\ActionColumn',
                
            'contentOptions' => ['style' => 'width:260px;'],
                'header'=>'Acciones',
                'template' => '{delete}'],  
        ],
    ]); ?>


</div>
