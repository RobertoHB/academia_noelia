<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Asignaturas;
use yii\helpers\ArrayHelper;
use app\models\Imparten;

/* @var $this yii\web\View */
/* @var $model app\models\Imparten */
/* @var $form yii\widgets\ActiveForm */


$idprof = $_GET['id'];
$nombreprof = $_GET['nombre'];
$apellidosprof = $_GET['apellidos'];
$items = ArrayHelper::map(Asignaturas::find()->all(), 'id','asignatura');
 
?>

<div class="imparten-form">

    <?php $form = ActiveForm::begin(); ?>

   <!--$form->field($model,'id')->textInput()--> 
    
   <?= $form->field($model, 'profesor')->hiddenInput(['value' => $idprof])->label(false);?>
   
    
    

    <?= $form->field($model, 'asignatura') ->dropDownList(
            $items,           // Flat array ('id'=>'label')
            ['prompt'=>'']);      // options; ?>
      
       
        

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
