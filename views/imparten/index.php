<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Imparten;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ImpartenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$setDatosProfesor  = new Imparten();
$setDatosProfesor->setIdProfesor($_GET['id']);
$setDatosProfesor->setNombreProfesor($_GET['nombre']);
$setDatosProfesor->setApellidosProfesor($_GET['apellidos']);

$this->title = 'Materias';
echo("<div style='float:right'><h3>".$setDatosProfesor->getNombreProfesor()." ".$setDatosProfesor->getApellidosProfesor()."</h3></div>");
//$this->params['breadcrumbs'][] = $this->profesores;
$this->params['breadcrumbs'][] = ['label' => 'profesores', 'url' => ['/profesores']];
?>
<div class="imparten-index">

    <!--<h1><?= Html::encode($datosProfesor) ?></h1>-->

    <p>
        <!--return Url::to(['imparten/index', 'id' => $model->id,'nombre'=>$model->nombre,'apellidos'=>$model->apellidos]);-->
       
        <?= Html::a('Añadir Materias', ['create','id' => $setDatosProfesor->getCodigoProfesor(),'nombre'=>$setDatosProfesor->getNombreProfesor(),'apellidos'=>$setDatosProfesor->getApellidosProfesor()], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'profesor',
            //'profesor0.nombre','profesor0.apellidos',
            'asignatura0.asignatura',
//            'asignatura',

            ['class' => 'yii\grid\ActionColumn',
                
            'contentOptions' => ['style' => 'width:260px;'],
        'header'=>'Acciones',
        'template' => '{delete}'],    

        ],
    ]); ?>


</div>
