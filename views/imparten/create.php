<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Imparten */

$this->title = 'Añadir Materias';
$this->params['breadcrumbs'][] = ['label' => 'Materias', 'url' => ['index','id' => $_GET['id'],'nombre'=>$_GET['nombre'],'apellidos'=>$_GET['apellidos']]];


$this->params['breadcrumbs'][] = $this->title;
echo("<div style='float:right'><h3>".$_GET['nombre']." ".$_GET['apellidos']."</h3></div>");
?>
<div class="imparten-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
      
    ]) ?>

</div>
