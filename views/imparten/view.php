<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Imparten */

$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Materias/profesor', 'url' => ['index','id'=>'profesor0.id','nombre'=>'profesor0.nombre','apellidos'=>'profesor0.apellidos']];
$this->params['breadcrumbs'][] = ['label' => 'Materias/profesor', 'url' =>Yii::$app->request->referrer ?: Yii::$app->homeUrl];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="imparten-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id,'nombre' => $model->profesor0], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'profesor0.nombre',
            'profesor0.apellidos',
            'asignatura',
        ],
    ]) ?>

</div>
