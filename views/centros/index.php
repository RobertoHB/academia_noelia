<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CentrosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Centros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="centros-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Centros', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'localidad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
