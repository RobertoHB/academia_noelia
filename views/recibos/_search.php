<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RecibosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recibos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>
    
    <?= $form->field($model, 'alumno') ?>
    
    <?= $form->field($model, 'matricula') ?>

    <?= $form->field($model, 'emision') ?>

    <?= $form->field($model, 'mes') ?>

    <?= $form->field($model, 'anyo') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'reducido') ?>

    <?php // echo $form->field($model, 'importe') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
