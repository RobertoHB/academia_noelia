<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\EmisionRecibos;
use dosamigos\datepicker\DatePicker;




/* @var $this yii\web\View */
/* @var $model app\models\Recibos */

//$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//
//echo"<pre>";
//var_dump($resultados);
//echo"</pre>";
//exit;






$anyos = ['2019'=>'2019','2020'=>'2020','2021'=>'2021','2022'=>'2022','2023'=>'2023','2024'=>'2024','2025'=>'2025','2026'=>'2026','2027'=>'2027','2028'=>'2028','2029'=>'2029','2030'=>'2030','2031'=>'2031','2032'=>'2032','2033'=>'2033'];
?>
<div class="jumbotron-fluid text-center bg-success">
  <h1>Impresión de Recibos Mensuales </h1>
 
</div>

<div class="container" style="margin-left: 290px;margin-top:50px;border-radius:5px;border:1px solid grey;width: 530px;">
<div class="recibos-view">
    <?php 
    if($mensaje != Null){?>
    <div class="col-sm-10 alert alert-danger text-center">
        <button class="close" data-dismiss="alert" ><span>&times;</span></button>
        <?= $mensaje ?>
    </div>
    <?php }
    ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="form-group col-md-12">
        
        <?php $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'formulario',
            'enableClientValidation' => true,
            'enableAjaxValidation' => false,
            'action'=>'recibospdf',
           ]); ?>
           
         <?= $form->field($model, 'fechaEmision')->widget(DatePicker::className(), [
            // inline too, not bad
            'inline' => false, 
            'options' => ['placeholder' => 'Seleccione una fecha de emisión'],
//            'template' => '{addon}{input}',que aparezca el calendario en la parte izda del input
            'containerOptions' => ['style' => 'width:400px;'],
       
             // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'dd-mm-yyyy',
                'todayBtn' => true,
                'todayHighlight' => true,
               
            ]
        ])->label('Fecha');?>
        
               <!--$options=['class'=>'form-control form-control-lg', 'style'=>'width:300px;text-align:center'])--> 
    
       <?= $form->field($model,'mesRecibo')->dropDownList(
               array(''=>'','1'=>'Enero','2'=>'Febrero','3'=>'Marzo','4'=>'Abril','5'=>'Mayo',
                     '6'=>'Junio','7'=>'Julio','8'=>'Agosto','9'=>'Septiembre','10'=>'Octubre',
                     '11'=>'Noviembre','12'=>'Diciembre'),
               $options=['class'=>'form-control form-control-lg', 'style'=>'width:400px'])->label('Mes');?>

        <?= $form->field($model,'anyoRecibo')->dropDownList(
                array(''=>'','2019'=>'2019','2020'=>'2020','2021'=>'2021','2022'=>'2022','2023'=>'2023','2024'=>'2024',
                      '2025'=>'2025','2026'=>'2026','2027'=>'2027','2028'=>'2028','2029'=>'2029',
                      '2030'=>'2030','2031'=>'2031','2032'=>'2032','2033'=>'2033'),
                $options=['class'=>'form-control form-control-lg', 'style'=>'width:400px'])->label('Año');?>
       

    </div>   
        <div class="form-group col-md-12">
            <?= Html::submitButton("Buscar", ["class" => "btn btn-primary","style"=>"width:400px;font-size:20px"]) ?>
    <!--            
           Html::submitButton('Emitir',
                    ['class'=>'btn btn-primary btn-lg active',

                        'style'=>'width:300px',
                        'data-toggle'=>'tooltip',
                        'title'=>Yii::t('app', 'Emisión de Recibos Mensual'),
                    ]
                )-->
        </div>
    </div>
    
 </div>   
 <?php ActiveForm::end(); ?>

 
<div class="recibos-index">
    <?php 
       
       
       if(isset($datos)){
        GridView::widget([        
            'dataProvider' => $datos,
            //'filterModel' => $searchModel,
            'columns' => $campos,
        ]); 
       }
        
      
           
//            ['class' => 'yii\grid\ActionColumn',
//                 'contentOptions'=>['style'=>'width: 70px;'],
//                
//            ],
//            
//               [
//                'class' => 'yii\grid\ActionColumn',
//                'template' => '{agrupaciones}',
//                'contentOptions'=>['style'=>'width: 30px;'],
//                'buttons' => [
//                'agrupaciones' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-user"></span>', $url, [
//                                        'title' => Yii::t('app', 'Agrupan'),
//                                    ]);
//
//                                }, 
//                                
//                ],
//                        
//                'urlCreator' => function ($action, $model, $key, $index) {
//                     return Url::to(['agrupan/index', 'id' => $model->id,'nombre'=>$model->nombre,'apellidos'=>$model->apellidos]);
//                }        
//                 
//            ],    
//            [
//              'class' => 'yii\grid\ActionColumn',
//              'template' => '{matriculas}',
//              'contentOptions'=>['style'=>'width: 40px;'],
//              'buttons' => [
//              'matriculas' => function ($url, $model) {
//                                  return Html::a('<span class="glyphicon glyphicon-bookmark"></span>', $url, [
//                                      'title' => Yii::t('app', 'Matriculas'),
//                                  ]);
//
//                              }, 
//
//                             ],
//
//              'urlCreator' => function ($action, $model, $key, $index) {
//                   return Url::to(['matriculas/matriculas_alumno', 'alumno' => $model->id,'nombre'=>$model->nombre,'apellidos'=>$model->apellidos]);
//              }        
//
//            ],          
//            
// 
    ?>
</div>