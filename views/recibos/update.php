<?php

use yii\helpers\Html;
use app\models\Recibos;
/* @var $this yii\web\View */
/* @var $model app\models\Recibos */

$this->title = 'Actualizar Recibo: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' =>Yii::$app->request->referrer ?: Yii::$app->homeUrl];

//print_r($_POST);



//si viene de exportar datos
//$this->params['breadcrumbs'][] = ['label' => 'Volver', 'url' => ['re', "mensaje" => $mensaje,"mesEmi" => $model->mes,"anyoEmi" => $model->anyo]];
         
if (isset($_POST['_csrf'])){
    $atras = -2;
}else{
    $atras = -1;
}
    echo Html::button('Volver', array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'style' => 'width:100px;',
//            'onclick' => 'document.location = "exportar_recibos"',
            'onclick' => 'history.go('.$atras.');return false;'
//            'data-method' => 'POST',
//            'data-params' => array("enviar_datos" =>1,
////                                "mesFin" =>$_POST['mesFin'],
////                                "anyo" =>$_POST['anyo'],
////                                "alumno" =>$_POST['alumno'],
////                                "tipo" =>$_POST['tipo'],
////                                "formato" =>$_POST['formato'],
////                            ),
//                            
//                ),
        ),
    );







//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recibos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formEmitidos', [
        'model' => $model,
    ]) ?>

</div>


