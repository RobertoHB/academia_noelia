<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\data\SqlDataProvider;
use app\models\Matriculas;
use app\models\Alumnos;
use app\models\Recibos;
use dosamigos\datepicker\DatePicker;

 $itemsAlumnos =  ArrayHelper::map(Alumnos::find()->select(['apellidos','nombre','id'])->asArray()->orderBy('apellidos')->all(),'apellidos', function ($model) {
                        return $model['apellidos'] .' '. $model['nombre'];
                        });
/* @var $this yii\web\View */
/* @var $searchModel app\models\RecibosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recibos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Recibos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
        
// echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
       
//        'datos' => $datos,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
//
            [
               'label'=>'Recibo Nº', 
               'attribute'=> 'id',
                'headerOptions' => ['style' => 'width:50px;'],
                  'enableSorting' => true,
            ],
            
//            
//            ['label'=>'Apellidos',
//             'attribute'=>'alumno0.apellidos',
//             'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id','apellidos'),
//             //'value' => '.apellidos',
//             'enableSorting' => true,  
             
             //'headerOptions' => ['style' => 'width:100px;background-color:grey; color: white;'],
//            ],
            
//            ['label'=>'Nombre',
//             'attribute'=>'alumno0.nombre',
//              //'value' => 'datosalumno',
//             'enableSorting' => true,  
//             
//             'headerOptions' => ['style' => 'width:100px;background-color:grey; color: white;'],
//            ],
            
//            [
//              'label' => 'Apellidos',
//              'attribute' => 'alumno0.apellidos',
//              'enableSorting' => true,
//              //'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id','apellidos'),
//              'headerOptions' => ['style' => 'width:150px;background-color:grey; color: white;'],
//            ],
            
            //'apelnomb',
            
             [
              'label' => 'Nombre',
              'attribute' => 'alumno0.nombre',
              'value' => 'alumno0.nombre',
              //'filter' => $itemsAlumnos, 
              'enableSorting' => true,
              'headerOptions' => ['style' => 'width:100px;background-color:grey; color: white;'],
            ],
//            
            
            [
              'label' => 'Apellidos',
              'attribute' => 'filtro_alumno',
              'value' => 'alumno0.apellidos',
              'filter' => $itemsAlumnos,
              'enableSorting' => true,
              'headerOptions' => ['style' => 'width:100px;background-color:grey; color: white;'],
            ],
            
//             [
//                'header' => 'Alumno',
//                'format' => 'raw',
//                'value'=>function ($model) {
//                    return $model->alumno0.apellidos  . ' ' .$model->alumno0.nombre;
//                    },
//             ],
//             [
//              'label' => 'Alumno',
//              'attribute' => 'alumno0',
//              'value' => ['alumno0.apellidos','alumno0.nombre'],
//              'filter' => $itemsAlumnos,
//              'enableSorting' => true,
//              'headerOptions' => ['style' => 'width:200px;background-color:grey; color: white;'],
//            ],
            
            
//               'matricula',
            [
               'label'=>'Matricula Nº', 
               'attribute'=> 'matricula',
                'headerOptions' => ['style' => 'width:50px;']
            ],
            
            ['label'=>'clase',
             'attribute'=>'matricula0.clase',   
            ],
            [
              'label' => 'Curso',
           // 'attribute' => 'curso0.curso',
           // 'value'=>'curso0.curso',
                
            'value' => function($model) {
                        foreach ($model->getCurso($model->id) as $curso) {
                            return $curso['curso'];
                        }
                    },
                                    
                                    
              'enableSorting' => true,
             
              'headerOptions' => ['style' => 'width:50px;background-color:grey; color: white;'],
            ],
            [
              'label' => 'Asignatura',
            //'attribute' => 
              'value' => function($model) {
                        foreach ($model->getAsignatura($model->id) as $asignatura) {
                            return $asignatura['asignatura'];
                        }
                    },  
              'enableSorting' => true,
              'headerOptions' => ['style' => 'width:50px;background-color:grey; color: white;'],
            ],
         
            
            
           // 'emision',
             [
                'attribute' => 'emision',
                'value'=>'emision',
                'format'=>'raw',
                'headerOptions' => ['style' => 'width:200px;'],
                'filter' => DatePicker::widget([
                   'model'=>$searchModel,
                   'attribute'=>'emision',
                    'inline' =>false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ] 
                ]),
            ],
            
            
            
            //'mes',
             [
               'label'=>'Mes', 
               'attribute'=> 'mes',
                'headerOptions' => ['style' => 'width:65px;']
            ],
            
            //'año',
             [
               'label'=>'Año', 
               'attribute'=> 'anyo',
                'headerOptions' => ['style' => 'width:100px;']
            ],
            //'estado',
            
            [
             'label'=>'Estado',
             'attribute'=>'estado',
             'format'=>'raw',
             'headerOptions' => ['style'=>'text-align:center'],
             'contentOptions' => function ($model) {
                             return ['style' => 'color:' 
                                 .($model->estado === 0 ? 'red' : 'blue')];
                            },
             'filter' => [0=>'Pendiente',1=>'Pagado'],  
             'headerOptions' => ['style' => 'width:120px;'],
             
             //'contentOptions' => [0=>['style'=>'color:red'],1=>['style'=>'color:blue']],   
             'value' => function($model) {
                            return $model->estado == 1 ? 'Pagado' : 'Pendiente';}             
            ],
           // 'reducido',
            'importe',

            ['class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'width:70px;'],
                        'header'=>'Acciones'
            ],
            
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{recibo_individual}',
            'contentOptions'=>['style'=>'width: 30px;'],
            'buttons' => [
            'recibo_individual' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-print"></span>', $url, [
                                    'title' => Yii::t('app', 'Imprimir recibo'),'target' => '_blank'
                                ]);
                            }, 

            ],

            'urlCreator' => function ($action, $model, $key, $index) {
                 return Url::to(['recibos/recibospdf', 'matricula' => $model->matricula,'mes' => $model->mes,'anyo' => $model->anyo,]);
            }        
                 
            ],         
                    
              
        ],
    ]); ?>


</div>
