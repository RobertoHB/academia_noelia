<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RecibosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
 //var_dump($model->mesRecibo);
$this->title = 'Recibos Emitidos';
$this->params['breadcrumbs'][] = $this->title;


//var_dump($parametros);
//exit;



if (!empty($_POST)){    
    $mes = $_POST["EmisionRecibos"]["mesRecibo"];
    $anyo = $_POST["EmisionRecibos"]["anyoRecibo"];
}else{
    $mes = $parametros['mes'];
    $anyo = $parametros['anyo'];
}



//echo("el mes es: ".$mrecibo." Y el anyo es: ".$arecibo);
//exit;

//echo("El mes del recibo es el: ". $mes_recibo);
//exit;

?>
<div class="recibos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Recibos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $datos,
      
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'matricula',
            'emision',
            'mes',
            'anyo',
            'estado',
            'reducido',
            'importe',
            'clase',
            'alumno',
            
           ['class' => 'yii\grid\ActionColumn',
            'template' => '{recibos}',
            'contentOptions'=>['style'=>'width: 30px;'],
            'buttons' => [
            'recibos' => function ($url, $datos) {
                                return Html::a('<span class="glyphicon glyphicon-tags"></span>', $url, [
                                    'title' => Yii::t('app', 'Recibos'),
                                ]);
                            }, 
            ],

            'urlCreator' => function ($action, $datos, $mes,$anyo) {
                 return Url::to(['recibos/update_emitidos', 'id' => $datos['id'],'mes'=>$mes,
                                'anyo'=>$anyo]);
                    
            }        
                 
            ],  
                    
        ],
    ]); ?>


</div>
