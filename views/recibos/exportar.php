<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Alumnos;
use app\models\EmisionRecibos;
use app\models\ExportacionRecibos;
use dosamigos\datepicker\DatePicker;
use yii\bootstrap\Modal;




/* @var $this yii\web\View */
/* @var $model app\models\Recibos */

//$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//
//echo"<pre>";
//var_dump($resultados);
//echo"</pre>";
//exit;

//print_r($_REQUEST);




$itemstipo = [''=>'',0=>'Recibos pendientes',1=>'Recibos Pagados'];
$itemsformato =  [1=>'Pantalla',2=>'Excel',3=>'Pdf'];
$itemsmeses =  array(''=>'','1'=>'Enero','2'=>'Febrero','3'=>'Marzo','4'=>'Abril','5'=>'Mayo',
                     '6'=>'Junio','7'=>'Julio','8'=>'Agosto','9'=>'Septiembre','10'=>'Octubre',
                     '11'=>'Noviembre','12'=>'Diciembre');
$itemsAlumnos =  ArrayHelper::map(Alumnos::find()->select(['id','apellidos','nombre'])->asArray()->orderBy('apellidos')->all(),'id', function ($model) {
                        return $model['apellidos'] .' '. $model['nombre'];
                        });
               // print_r($_POST);
                        
  
                        
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
<div class="jumbotron-fluid text-center bg-success">
  <h1>Generación de  Informes</h1>
 
</div>

<div class="container col-md-12" style="margin-left: 0px;margin-top:50px;">

    <?php 
    if($mensaje != Null){?>
    <div class="col-sm-10 alert alert-danger text-center">
        <button class="close" data-dismiss="alert" ><span>&times;</span></button>
        <?= $mensaje ?>
    </div>
    <?php }
    
    
    
    ?>
    <h1><?= Html::encode($this->title) ?></h1>
   
    <div class="form-group col-md-4" style="border-radius:5px;border:1px solid grey;width: 250px;padding:15px;margin:15px;">
        
        <?php $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'formulario',
            'enableClientValidation' => true,
            'enableAjaxValidation' => false,
            'action'=>'exportar_recibos',
           ]); ?>
           
      
        <?= $form->field($model,'mesIni')->dropDownList(
              $itemsmeses,
              $options=['class'=>'form-control form-control-lg', 'style'=>'width:200px'])->label('Mes Inicial');?>
        
        
        <?= $form->field($model,'mesFin')->dropDownList(
             $itemsmeses,
             $options=['class'=>'form-control form-control-lg', 'style'=>'width:200px'])->label('Mes Final');?>

        <?= $form->field($model,'anyoRecibo')->dropDownList(
                array(''=>'','2019'=>'2019','2020'=>'2020','2021'=>'2021','2022'=>'2022','2023'=>'2023','2024'=>'2024',
                      '2025'=>'2025','2026'=>'2026','2027'=>'2027','2028'=>'2028','2029'=>'2029',
                      '2030'=>'2030','2031'=>'2031','2032'=>'2032','2033'=>'2033'),
                $options=['style'=>'width:200px'])->label('Año');?>
        
              
    
         <?= $form->field($model,'alumno')->dropDownList(
               $itemsAlumnos,['prompt'=>''],
               $options=['class'=>'form-control form-control-lg', 'style'=>'width:200px'])->label('Alumno');?>
         
        
        
        <?= $form->field($model,'tipo')->dropDownList(
          $itemstipo,           // Flat array ('id'=>'label')
            $options=['style'=>'width:200px'])->label('Tipo');      // options; ?>     
              

        <?= $form->field($model,'formato')->dropDownList(
         $itemsformato,           // Flat array ('id'=>'label')
          $options=['style'=>'width:200px'])->label('Formato');      // options; ?>             
          
      
        <?= Html::submitButton("Consultar", ["class" => "btn btn-primary","id" => "consultar","style"=>"width:200px;font-size:20px;margin-top:20px;"],[]) ?>

       
   
    

 <?php ActiveForm::end(); ?>
 </div>
 
    <div class="form-group col-md-8" style="margin-left: 30px;height: 500px;overflow: auto;">
    <?php 
       
       
       if(isset($datos)){
         
        echo (GridView::widget([        
            'dataProvider' => $datos,
             'showFooter' => true,
           
            // 'showPageSummary' => true,
           //'filterModel' => $searchModel,
          'columns' =>[
                ['class' => 'yii\grid\SerialColumn'],
                [
                  'attribute'=>'recibo',
                  'value'=>'recibo'
                ],
                [
                  'attribute'=>'matricula',
                  'value'=>'matricula'
                ],
                [
                  'attribute'=>'nombre',
                  'value'=>'nombre'
                ],
               [
                  'attribute'=>'apellidos',
                  'value'=>'apellidos'
                ],
               [
                  'attribute'=>'mes',
                  'value'=>'mes'
                ],
               [
                  'attribute'=>'anyo',
                  'value'=>'anyo'
                ],
           
                [
                  'attribute'=>'importe',
                  'contentOptions' => ['style'=>'padding:0px 0px 0px 30px;vertical-align: middle;'],
                  'footer' => $total,      
                 
                ],
//                ['class' => 'yii\grid\ActionColumn',
//                    'contentOptions' => ['style' => 'width:70px;'],
//                        'header'=>'Acciones'
//            ],
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{recibo_update}',
            'contentOptions'=>['style'=>'width: 30px;'],
            'buttons' => [
             'recibo_update' => function ($url, $model) {
                                return Html::button('<span class="glyphicon glyphicon-pencil"></span>',
                                        ['value'=>'/academianoelia/web/index.php/recibos/actualizar_recibos'.'?id='.$model['recibo'],'id'=>'modal_actualiza_recibo','style'=>'border:none;background-color:transparent;'],
//                                        [
//                                            'data' => [
//                                                'method' => 'post',
//                                                 // use it if you want to confirm the action
//                                                 //'confirm' => 'Estas seguro de eliminar?',
//                                             ],
//                                             'class' => 'glyphicon glyphicon-pencil custom_button'
//                                        ]
   
                                        );
                            }, 
                                    

                        ],
                                    
                 
            ],  
                                    
                                    
                                    
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{recibo_delete}',
            'contentOptions'=>['style'=>'width: 30px;'],
            'buttons' => [
            'recibo_delete' => function ($url, $model) {
                                return Html::a('',
                                        ['recibos/delete', 'id' => $model['recibo']],
                                        [
                                            'data' => [
                                                'method' => 'post',
                                                 // use it if you want to confirm the action
                                                 'confirm' => 'Estas seguro de eliminar?',
                                             ],
                                             'class' => 'glyphicon glyphicon-trash custom_button'
                                        ]
   
                                        );
                            }, 
   
                        ],

                 
            ],                                      
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{recibo_individual}',
            'contentOptions'=>['style'=>'width: 30px;'],
            'buttons' => [
            'recibo_individual' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-print"></span>',
            ['recibos/recibospdf', 'matricula' => $model['matricula'],'mes' => $model['mes'],'anyo' => $model['anyo']],['target'=>'_blank']);
                            }, 
                                    

                        ],

                 
            ],    
                //['class' => 'yii\grid\ActionColumn'],
              ],
           
        ])); 
       }
        
      

    ?>
</div>
    
   </div> 


<!--ventana modal para actualizar recibos-->
  <div id="modal_actualizar_recibo">
         <?php Modal::begin([
            'id'=>'modal_act_recibos',
            'size'=>'modal-lg',
            'closeButton' => [
                'id'=>'close-button',
                'class'=>'close',
                'data-dismiss' =>'modal',
                ],
            'header' => '<h2>Actualizacion de Recibos </h2>',
                      //'toggleButton' => ['label' => 'click me'],
              ]);

                 echo "<div id='modalContent'></div>";

           Modal::end();?>


  </div>
<script>
   $( document ).ready(function() {
         if (window.history.replaceState) { // verificamos disponibilidad
    window.history.replaceState(null, null, window.location.href);
       }
       
       $('#modal_act_recibos').on('hidden.bs.modal', function () {
            $('#consultar').click();
        });
      
       
       $(document).on("click", "#modal_actualiza_recibo", function () {
//        console.log("mostrar modal")
         $('#modal_act_recibos').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
    }); 
       
       
    });
    
    
  


</script>
<!--<script>         
    window.onload = function() {
//        if($.POST != ''){    
//            //console.log("me cago en tus muertos");
//            document.getElementById('consultar').click();        
//        }
    }     
           
</script>;-->