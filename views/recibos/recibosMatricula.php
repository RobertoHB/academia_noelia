<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\data\SqlDataProvider;
use app\models\Matriculas;
use app\models\Alumnos;
use app\models\Recibos;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RecibosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
echo Html::button('Volver', array(
            'name' => 'btnBack',
            'class' => 'uibutton loading confirm',
            'style' => 'width:100px;',
            'onclick' => "history.go(-1)",
                )
        );
//$this->title = 'Recibos';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="recibos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Recibos', ['create','matricula' =>$_GET['matricula']], ['class' => 'btn btn-success']) ?>
      
    </p>

    <?php 
        
// echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
       
//        'datos' => $datos,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
//
            [
               'label'=>'Recibo Nº', 
               'attribute'=> 'id',
                'headerOptions' => ['style' => 'width:50px;'],
                  'enableSorting' => true,
            ],
            
            
            ['label'=>'Nombre',
             'attribute'=>'alumno0.nombre',   
             'enableSorting' => true,  
             
             'headerOptions' => ['style' => 'width:100px;background-color:grey; color: white;'],
            ],
    
            
            [
              'label' => 'Apellidos',
              'attribute' => 'alumno0.apellidos',
              'enableSorting' => true,
              //'filter' => ArrayHelper::map(Alumnos::find()->all(), 'id','apellidos'),
              'headerOptions' => ['style' => 'width:150px;background-color:grey; color: white;'],
            ],
            
            
            
//               'matricula',
            [
               'label'=>'Matricula Nº', 
               'attribute'=> 'matricula',
                'headerOptions' => ['style' => 'width:50px;']
            ],
            
            ['label'=>'clase',
             'attribute'=>'matricula0.clase',   
            ],
            [
              'label' => 'Curso',
           // 'attribute' => 'curso0.curso',
           // 'value'=>'curso0.curso',
                
            'value' => function($model) {
                        foreach ($model->getCurso($model->id) as $curso) {
                            return $curso['curso'];
                        }
                    },
                                    
                                    
              'enableSorting' => true,
             
              'headerOptions' => ['style' => 'width:50px;background-color:grey; color: white;'],
            ],
            [
              'label' => 'Asignatura',
            //'attribute' => 
              'value' => function($model) {
                        foreach ($model->getAsignatura($model->id) as $asignatura) {
                            return $asignatura['asignatura'];
                        }
                    },  
              'enableSorting' => true,
              'headerOptions' => ['style' => 'width:50px;background-color:grey; color: white;'],
            ],
         
            
            
           // 'emision',
             [
                'attribute' => 'emision',
                'value'=>'emision',
                'format'=>'raw',
                'headerOptions' => ['style' => 'width:200px;'],
                'filter' => DatePicker::widget([
                   'model'=>$searchModel,
                   'attribute'=>'emision',
                    'inline' =>false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ] 
                ]),
            ],
            
            
            
            //'mes',
             [
               'label'=>'Mes', 
               'attribute'=> 'mes',
                'headerOptions' => ['style' => 'width:50px;']
            ],
            
            //'año',
             [
               'label'=>'Año', 
               'attribute'=> 'anyo',
                'headerOptions' => ['style' => 'width:90px;']
            ],
            //'estado',
            
            [
             'label'=>'Estado',
             'attribute'=>'estado',
             'format'=>'raw',
             'headerOptions' => ['style'=>'text-align:center'],
             'contentOptions' => function ($model) {
                             return ['style' => 'color:' 
                                 .($model->estado === 0 ? 'red' : 'blue')];
                            },
             'filter' => [0=>'Pendiente',1=>'Pagado'],  
             'headerOptions' => ['style' => 'width:120px;'],
             
             //'contentOptions' => [0=>['style'=>'color:red'],1=>['style'=>'color:blue']],   
             'value' => function($model) {
                            return $model->estado == 1 ? 'Pagado' : 'Pendiente';}             
            ],
           // 'reducido',
            'importe',

            ['class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'width:70px;'],
                        'header'=>'Acciones'],
              
        ],
    ]); ?>


</div>
