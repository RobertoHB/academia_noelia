<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Recibos */
/* @var $form yii\widgets\ActiveForm */

//$this->title = 'Nuevo Recibo';
$estadoRecibo = [0 => 'Pendiente', 1 => 'Pagado'];

if(isset($_GET['matricula']))  echo("<div style='with:100%;font-size:40px;float:right;'>Matricula Nº ".$_GET['matricula']."</div>"); 
//$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' =>Yii::$app->request->referrer ?: Yii::$app->homeUrl];



?>


<div class="recibos-form">
    
    
    <?php $form = ActiveForm::begin(); ?>
    <?= (isset($_GET['matricula'])) ? $form->field($model, 'matricula')->hiddenInput(['value'=>$_GET['matricula']])->label(''):
        $form->field($model, 'matricula')->textInput();?> 
      

    

     <!--$form->field($model, 'emision')->textInput()--> 
    <?= $form->field($model, 'emision')->widget(DatePicker::className(), [
          // inline too, not bad
          'inline' => false, 
           // modify template for custom rendering
          //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
          'clientOptions' => [
          'autoclose' => true,
          'format' => 'dd-mm-yyyy',
          'todayBtn' => true
          ]
    ]);?>
    
    
    
    
    

    <?= $form->field($model, 'mes')->textInput() ?>

    <?= $form->field($model, 'anyo')->textInput()->label('A�o') ?>

   
     
    <?= $form->field($model, 'estado')->dropDownList($estadoRecibo, ['prompt' => 'Seleccione Uno' ]); ?>
     
    <?= $form->field($model, 'reducido')->textInput() ?>

    <?= $form->field($model, 'importe')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
 
    <?php ActiveForm::end(); ?>

</div>
