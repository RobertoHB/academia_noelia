<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Recibos */
/* @var $form yii\widgets\ActiveForm */

//$this->title = 'Nuevo Recibo';
$estadoRecibo = [0 => 'Pendiente', 1 => 'Pagado'];

echo("<div style='with:100%;font-size:20px;margin-bottom:10px;background-color:grey;color:white;'>Recibo nº ".$model->id."</div>"); 
//$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' =>Yii::$app->request->referrer ?: Yii::$app->homeUrl];

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>


<div style="width: 100%;text-align: center;"><label id="txt_guardado_db"></label></div>
<div class="recibos-form">
    
    
    <?php $form = ActiveForm::begin(); ?>
  
     <!--$form->field($model, 'emision')->textInput()--> 
    <?= $form->field($model, 'id')->hiddenInput()->label('') ?>
     
    <?= $form->field($model, 'emision')->widget(DatePicker::className(), [
          // inline too, not bad
          'inline' => false, 
           // modify template for custom rendering
          //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
          'clientOptions' => [
          'autoclose' => true,
          'format' => 'dd-mm-yyyy',
          'todayBtn' => true
          ]
    ]);?>
    
    
    
    
    

    <?= $form->field($model, 'mes')->textInput() ?>

    <?= $form->field($model, 'anyo')->textInput()->label('Año') ?>

   
     
    <?= $form->field($model, 'estado')->dropDownList($estadoRecibo, ['prompt' => 'Seleccione Uno' ]); ?>
     
     <!--$form->field($model, 'reducido')->textInput() ?>-->

    <?= $form->field($model, 'importe')->textInput() ?>

    <div class="form-group">
        <!--Html::Button('Actualizar', ['class' => 'btn btn-success'])--> 
          <?=Html::Button("Actualizar", ['class' => 'btn btn-success','id' => 'actualizar_recibo','title'=>'Actualizar'])?>
    </div>
 
    <?php ActiveForm::end(); ?>

</div>





<script>
    
$( document ).ready(function() {

    $('#actualizar_recibo').on('click', function(e) {
       

    $ .ajax ({
     url: "guardarcambios",
     method: "POST",
     data: $ (this.form) .serialize (),
    success: function (data) {
        
        if(data){
            $("#txt_guardado_db").text("Guardado");
            for(let i=0;i<15;i++){
                $("#txt_guardado_db").fadeToggle(500);    
            }   
             $("#close-button").click();
            
        }
      
//      
     



    },

    error: function () {

        alert("Error");

    }
    });
});    

});
</script>