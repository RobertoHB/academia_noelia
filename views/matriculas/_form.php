<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Matriculas;
use yii\widgets\DetailView;
use dosamigos\datepicker\DatePicker;
//echo("<pre>");
//var_dump($itemAlumno);
//echo("</pre>");
//exit;

/* @var $this yii\web\View */
/* @var $model app\models\Matriculas */
/* @var $form yii\widgets\ActiveForm */

   $model->alumno = $_GET['alumno']; 
   


?>
<div class="matriculas-form">
 <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'alumno',
            'alumno0.nombre',
            'alumno0.apellidos',
        ],
    ]) ?>
</div>



<div class="matriculas-form">

    <?php $form = ActiveForm::begin(); ?> 
    
   <?= ($_GET['alumno'] > 0) ? $form->field($model, 'alumno')->Input(['value'=>$_GET['alumno']])->label(''):
        $form->field($model, 'alumno')->dropDownList($itemAlumnos,['prompt'=>'']);?>
    
  
     <?= $form->field($model, 'clase') ->dropDownList($itemClases, // Flat array ('id'=>'label')
                                                    ['prompt'=>'']);  ?>   
    <!--$form->field($model, 'alta')->textInput() ?>-->
    <?= $form->field($model, 'alta')->widget(DatePicker::className(), [
            // inline too, not bad
            'inline' => false, 
             // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
            'todayBtn' => true
            ]
    ]);?>
    
     <!--$form->field($model, 'baja')->textInput() ?>-->
     <?= $form->field($model, 'baja')->widget(DatePicker::className(), [
            // inline too, not bad
            'inline' => false, 
             // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
            'todayBtn' => true,
            'value ' => null,    
            ]
    ]);?>

    <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
