<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Matriculas */

$this->title = 'Actualizar Matricula: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Matriculas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Volver', 'url' =>Yii::$app->request->referrer ?: Yii::$app->homeUrl];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="matriculas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formActualiza', [
        'model' => $model,
        'itemClases' => $itemClases,
        'itemAlumnos' => $itemAlumnos,
    ]) ?>

</div>
