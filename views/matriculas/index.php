<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Alumnos;
use app\models\Cursos;
use app\models\Asignaturas;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MatriculasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$lista_alumnos_apel =  ArrayHelper::map(Alumnos::find()->select(['id','apellidos','nombre'])->asArray()->orderBy('apellidos')->all(),'id', function ($model) {
                        return $model['apellidos'] .' - '. $model['nombre'];
                        });

$this->title = 'Matriculas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matriculas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Matriculas', ['create','alumno' => ''], ['class' => 'btn btn-success']) ?>
       
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
               'label'=>'Matricula', 
               'attribute'=> 'id',
                'headerOptions' => ['style' => 'width:30px;']
            ],
           //'alumno',
            [
              'label' => 'Nombre',
              'attribute' => 'alumno',
              'value' => 'alumno0.nombre',
              'filter' => false,
              'enableSorting' => true,
            ],
            [
              'label' => 'Apellidos',
              'attribute' => 'alumno',
              'value' => 'alumno0.apellidos',
              'filter' => $lista_alumnos_apel,
              'enableSorting' => true,
              'headerOptions' => ['style' => 'width:250px;']
            ],
            [
                'label' => 'Clase',
                'attribute' => 'clase',
                'headerOptions' => ['style' => 'width:10px']
            ],
//            ['clase',
//             'contentOptions' => ['style' => 'width:20px;']],
//          
           
            [
              'header' => 'Curso',
              'attribute' => 'curso',
              'headerOptions' => ['style' => 'background-color:grey; color: white;'],
              'value' => 'curso0.curso',
              'filter' => ArrayHelper::map(Cursos::find()->all(), 'id', 'curso'),
              'enableSorting' => true,
            ],
            [ 
            'attribute' => 'asignatura',
            'headerOptions' => ['style' => 'background-color:grey; color: white;'],
            'value' => 'asignatura0.asignatura',
            //'filter' => ArrayHelper::map(Asignaturas::find()->all(), 'id', 'asignatura'),
            'enableSorting' => true,
            ],
            [
                'attribute' => 'alta',
                'value'=>'alta',
                'format'=>'raw',
                'headerOptions' => ['style' => 'width:175px;'],
                'filter' => DatePicker::widget([
                   'model'=>$searchModel,
                   'attribute'=>'alta',
                    'inline' =>false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ]
                    
                    
                ]),
                
                
//                'format' => ['nullDisplay' => '',]
                
            ],
            [
                'attribute' => 'baja',
                'value' => 'baja',
                'format'=>'raw',
                'headerOptions' => ['style' => 'width:175px;'],
                'filter' => DatePicker::widget([
                   'model'=>$searchModel,
                   'attribute'=>'baja',
                    'inline' =>false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]),
                
            ],
            'observaciones:ntext',

            ['class' => 'yii\grid\ActionColumn',
                  'contentOptions' => ['style' => 'width:70px;'],
        'header'=>'Acciones'],
        ],
    ]); ?>


</div>
