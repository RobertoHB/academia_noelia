<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Cursos;
use app\models\Asignaturas;
use yii\helpers\ArrayHelper;



/* @var $this yii\web\View */
/* @var $searchModel app\models\ClasesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clases';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clases-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Clases', ['create'], ['class' => 'btn btn-success']) ?>
    
    </p>

    <?php    
   
    
    // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
             [
              'label' => 'Curso',
              'attribute' => 'curso',
               'value' => 'curso0.curso',
                'filter' => ArrayHelper::map(Cursos::find()->all(), 'id', 'curso'),
              'enableSorting' => true,
            ],
           // 'curso0.curso',
            [
              'label' => 'Asignatura',
              'attribute' => 'asignatura',
               'value' => 'asignatura0.asignatura',
                'filter' => ArrayHelper::map(asignaturas::find()->all(), 'id', 'asignatura'),
              'enableSorting' => true,
            ],
            //'asignatura0.asignatura',
             /* Con las funciones getter se puede usar la notación de punto para devolver un campo del registro que devuelve */
            /* SÓLO EN EL GRIDVIEW */
            /*'usuario0.nombre',*/
            'importe',
            'observaciones:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
