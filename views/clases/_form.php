<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Asignaturas;
use app\models\Cursos;
use yii\helpers\ArrayHelper;

$ListAsig = ArrayHelper::map(Asignaturas::find()->all(), 'id', 'asignatura');
$ListaCurs = ArrayHelper::map(Cursos::find()->all(), 'id','curso','descripcion');
/* @var $this yii\web\View */
/* @var $model app\models\Clases */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clases-form">

    <?php $form = ActiveForm::begin(); ?>

     <!--$form->field($model, 'curso')->textInput()--> 
     <!--$form->field($model, 'asignatura')->textInput()--> 
    <?= $form->field($model, 'curso')->dropDownList($ListaCurs, ['prompt' => 'Seleccione Uno' ]); ?>
    <?= $form->field($model, 'asignatura')->dropDownList($ListAsig, ['prompt' => 'Seleccione Uno' ]); ?>
    
    <?= $form->field($model, 'importe')->textInput() ?>

    <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
