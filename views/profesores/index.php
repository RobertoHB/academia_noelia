<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfesoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Profesores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Profesores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php   // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'apellidos',
            'direccion',
            'poblacion',
            //'dni',
            //'movil',
            //'fijo',
            //'email:email',
            //'alta',
            //'baja',
            //'observaciones:ntext',

            ['class' => 'yii\grid\ActionColumn',
             'contentOptions'=>['style'=>'width: 70px;'],
//             'buttons'  => ['view' => function($url, $model) {
//                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
//                    	'title' => Yii::t('app', 'View'),]);
//
//                    }
//                 ]
             ],       
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{materias}',
                'buttons' => [
                'materias' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-book"></span>', $url, [
                                        'title' => Yii::t('app', 'Materias'),
                                    ]);

                            }        
                ],
                        
                'urlCreator' => function ($action, $model, $key, $index) {
                     return Url::to(['imparten/index', 'id' => $model->id,'nombre'=>$model->nombre,'apellidos'=>$model->apellidos]);
                }        
                 
            ],    
            
           
            
        ],
    ]); ?>


</div>
