<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use app\models\Alumnos;

/* @var $this yii\web\View */
/* @var $model app\models\Material */
/* @var $form yii\widgets\ActiveForm */
$items = ArrayHelper::map(Alumnos::find()->select(['id','apellidos','nombre'])->asArray()->orderBy('apellidos')->all(),'id', function ($model) {
                        return $model['apellidos'] .' - '. $model['nombre'];
                        });
?>

<div class="material-form">

    <?php $form = ActiveForm::begin(); ?>

  
    <?= $form->field($model, 'fecha')->widget(DatePicker::className(), [
         // inline too, not bad
         'inline' => false, 
          // modify template for custom rendering
         //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
         'clientOptions' => [
         'autoclose' => true,
         'format' => 'yyyy-mm-dd',
         'todayBtn' => true
         ]
    ]);?>
    
    

    <?= $form->field($model, 'alumno')->dropDownList(
          $items,           // Flat array ('id'=>'label')
          ['prompt'=>'']);      // options; ?>
    
    

    <?= $form->field($model, 'concepto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'importe')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
