<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cuentas_bancariasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuentas-bancarias-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'alumno') ?>

    <?= $form->field($model, 'iban') ?>

    <?= $form->field($model, 'entidad') ?>

    <?= $form->field($model, 'oficina') ?>

    <?php // echo $form->field($model, 'dc') ?>

    <?php // echo $form->field($model, 'cuenta') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
