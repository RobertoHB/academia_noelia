<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Cuentas_bancariasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cuentas Bancarias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuentas-bancarias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Cuentas Bancarias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'alumno',
            'iban',
            'entidad',
            'oficina',
            //'dc',
            //'cuenta',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
