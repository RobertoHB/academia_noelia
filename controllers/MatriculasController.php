<?php

namespace app\controllers;

use Yii;
use app\models\Matriculas;
use app\models\MatriculasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\Clases;
use app\models\Alumnos;



/**
 * MatriculasController implements the CRUD actions for Matriculas model.
 */
class MatriculasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Matriculas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MatriculasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Matriculas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Matriculas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($alumno)
    {
        $model = new Matriculas();
        $itemsClases = ArrayHelper::map(Clases::find()->all(), 'id','asignatura0.asignatura','curso0.curso');
        
//        $itemsAlumnos = ArrayHelper::map(Alumnos::find()->asArray()->select('id,apellidos,nombre')->orderBy('apellidos')->all(),'id','apellidos','nombre');
     
        $itemsAlumnos =  ArrayHelper::map(Alumnos::find()->select(['id','apellidos','nombre'])->asArray()->orderBy('apellidos')->all(),'id', function ($model) {
                        return $model['apellidos'] .' - '. $model['nombre'];
                        });
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $datosAlumno = ArrayHelper::toArray(Alumnos::find()
                ->select('id,nombre,apellidos')
                ->where(['id' => $model->alumno])->all());
            //return $this->redirect(['view', 'id' => $model->id]);
            //return $this->redirect(['url' =>Yii::$app->request->referrer ?: Yii::$app->homeUrl]);
            return $this->redirect(['matriculas_alumno', 'alumno' =>$datosAlumno[0]['id'],'nombre'=>$datosAlumno[0]['nombre'],'apellidos'=>$datosAlumno[0]['apellidos']]);
             //return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            
        }

        return $this->render('create', [
            'model' => $model,
            'itemClases' => $itemsClases,
            'itemAlumnos' => $itemsAlumnos,
        ]);
    }

    /**
     * Updates an existing Matriculas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $itemsClases = ArrayHelper::map(Clases::find()->all(), 'id','asignatura0.asignatura','curso0.curso');
//        $itemsAlumnos = ArrayHelper::map(Alumnos::find()->all(),'id','apellidos','nombre');
        $itemsAlumnos =  ArrayHelper::map(Alumnos::find()->select(['id','apellidos','nombre'])->asArray()->orderBy('apellidos')->all(),'id', function ($model) {
                        return $model['apellidos'] .' - '. $model['nombre'];
                        });
        $model = $this->findModel($id);
         
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'itemClases' => $itemsClases,
            'itemAlumnos' => $itemsAlumnos,
          
        ]);
    }

    /**
     * Deletes an existing Matriculas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        //return $this->redirect(['index']);
    }
    
    
    
       public function actionMatriculas_alumno($alumno,$nombre,$apellidos)
    {
        
        $searchModel = new MatriculasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere("alumno = '$alumno'");
       

        return $this->render('matriculasAlumno', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Matriculas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Matriculas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Matriculas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
