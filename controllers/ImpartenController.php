<?php

namespace app\controllers;

use Yii;
use app\models\Imparten;
use app\models\ImpartenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * ImpartenController implements the CRUD actions for Imparten model.
 */
class ImpartenController extends Controller
{
    
    
    /**
     * {@inheritdoc}
     */
     
   
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Imparten models.
     * @return mixed
     */
    public function actionIndex($id,$nombre,$apellidos)
    {
        $searchModel = new ImpartenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere("profesor = '$id'");
       $datos = $nombre." ".$apellidos;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'datosProfesor' =>$datos,
            'idProfesor' =>$id,
        ]);
    }

    /**
     * Displays a single Imparten model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Imparten model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id,$nombre,$apellidos)
    {
        $model = new Imparten();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $id,'nombre'=>$nombre,'apellidos'=>$apellidos]);
        }

        return $this->render('create', [
            'model' => $model,
           
        ]);
    }

    /**
     * Updates an existing Imparten model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Imparten model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
  
       //volvemos a la ventana anterior con la misma url 
       return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
         //return $this->redirect(Yii::app()->request->urlReferrer);
        //return $this->redirect(['index','id' => $codigoProfesor,'nombre' => $nombreProfesor,'apellidos' => $apellidosProfesor]);
    }

        
    /**
     * Finds the Imparten model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Imparten the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Imparten::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
