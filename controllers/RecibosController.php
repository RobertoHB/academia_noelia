<?php
namespace app\controllers;
use Yii;
use yii\helpers\ArrayHelper;
use app\models\Recibos;
use app\models\RecibosSearch;
use app\models\Matriculas;
use app\models\Alumnos;
use app\models\Clases;
use app\models\Cursos;
use app\models\Asignaturas;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\EmisionRecibos;
use app\models\ExportacionRecibos;
use \Mpdf\Mpdf;
use yii\db\Query;
//use app\models\EmisionRecibos;
//use yii\db\Connection;


//$connection = new Connection([
//    'dsn' => 'academia_noelia',
//   
//    'username' => 'root',
//    'password' => 'eldeorejo',
//]);
//$connection->open();
        




/**
 * RecibosController implements the CRUD actions for Recibos model.
 */
class RecibosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recibos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecibosSearch();
        $datos = new Recibos();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'datos' => $datos,
        ]);
    }

    /**
     * Displays a single Recibos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recibos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recibos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
         //Consulta que devuelve los datos del alumno relacionados con la matricula
//         $datosAlumno = new SqlDataProvider([
//                             'sql' => "SELECT al.nombre nombre,al.apellidos apellidos, mat.id matri FROM matriculas mat JOIN 
//                                        alumnos al ON al.id = mat.alumno WHERE mat.id = $model->id"]);

        return $this->render('create', [
            'model' => $model,
//            'datosAlumno' => $datosAlumno,
        ]);
    }

    /**
     * Updates an existing Recibos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    public function actionActualizar_recibos($id)
    {
        $model = $this->findModel($id);

//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
//            
//        }else{    
            return $this->renderPartial('_formActualiza', [
            'model' => $model,
        ]);
//        }

        
    }
     public function actionGuardarcambios(){
        
        $recibo = $_POST["Recibos"]["id"];
        $emision = $_POST["Recibos"]["emision"];
        $mes = $_POST["Recibos"]["mes"];
        $anyo = $_POST["Recibos"]["anyo"];
        $estado = $_POST["Recibos"]["estado"];
        $importe = $_POST["Recibos"]["importe"];
        
       
        $model = Recibos::find()->where(['id' => $recibo])->one();
         if ($model->validate()) {
            $model->emision = $emision;
            $model->mes = $mes;
            $model->anyo = $anyo;
            $model->estado = $estado;
            $model->importe = $importe;
            $model->update();
            return true;
         }
         //$attributes = ['emision'=>$emision,'mes'=>$mes,'anyo'=>$anyo,'estado'=>$estado,'importe'=>$importe];
      
        
//        $model->updateAttributes($attributes);
      
       
       
       
            
         
    }
    
    
     
    
    
     public function actionConsultar_emitidos()
    {
        $mensaje="";
        $model = new EmisionRecibos();
        return $this->render('consultarRecibos', [
            'model' => $model,
            'mensaje' => $mensaje,
        ]);
    }

     public function actionUpdate_emitidos($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('updateEmitidos', [
            'model' => $model,
        ]);
    }
    
    
    /**
     * Deletes an existing Recibos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

     public function actionEliminar_recibos($mes=NULL,$anyo=NULL)
    {
         
        echo ("El mes a eliminar es el : ".$mes." Y el año: ".$anyo); 
        exit;

        return $this->redirect(['index']);
    }
    
    
    public function actionRecibos_matricula($matricula){
        $searchModel = new RecibosSearch();
        //$datos = new Recibos();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere("matricula = '$matricula'");
        
       
        

        return $this->render('recibosMatricula', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
           
            //'datos' => $datos,
        ]);
  
    }
    
    
    /**
     * Finds the Recibos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recibos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recibos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
   
    
    //Emision de recibos. Criterios de la consulta: Matriculas en activo, es decir, con fecha de baja vacia o nula. Se insertaran los valores de
     //Matricula-Emision(fecha en la que se emiten)-Mes(input)-A�o(input)-Estado(0=pendiente)-Reducido(%=porcentaje de descuento)-Importe(valor recogido del Importe de la clase)
    
    public function actionEmitir_recibos($mensaje = null){
        $model = new EmisionRecibos();
        return $this->render('emision',['model'=>$model,'mensaje'=>$mensaje]);
        
    }
    public function actionConsultar_recibos($mensaje = null){
        $model = new EmisionRecibos();
        return $this->render('filtrado',['model'=>$model,'mensaje'=>$mensaje]);
        
    }
    
//    public function actionExportar_recibos($mensaje = null){
//        
//        $model = new ExportacionRecibos();
//        return $this->render('exportar',['model'=>$model,'mensaje'=>$mensaje]);
//        
//    }
    
    
     public function actionExportar_recibos(){
         $mensaje = "";
        
     if(isset($_POST['ExportacionRecibos']['alumno'])){   
         
     if(($_POST['ExportacionRecibos']['alumno'] != Null)){
         $alumno = $_POST['ExportacionRecibos']['alumno'];
        $filtro_alumno = " and a.id = '$alumno'";
     }else{
          $filtro_alumno = "";
     }
     
     }else{
          $filtro_alumno = "";
     }
      
        $model = new ExportacionRecibos();
         
        
        $searchModel = new RecibosSearch();
        
        if ($model->load(Yii::$app->request->post())) {
            $mes_ini = $model->mesIni;
            $mes_fin = $model->mesFin;
            $anyo = $model->anyoRecibo;
            $tipo = $model->tipo;
            $formato = $model->formato;
            $mensaje = "";
            
             $db = Yii::$app->db;
             
            $count = $db->createCommand("SELECT count(*) FROM recibos r 
                            JOIN matriculas m  ON r.matricula = m.id join alumnos a  on a.id = m.alumno 
                            where (estado = '$tipo') and (mes BETWEEN '$mes_ini' AND '$mes_fin') and anyo = '$anyo' $filtro_alumno")->queryScalar();
            
             $suma_total = $db->createCommand("SELECT sum(importe) FROM recibos r 
                            JOIN matriculas m  ON r.matricula = m.id join alumnos a  on a.id = m.alumno 
                            where (estado = $tipo) and (mes BETWEEN $mes_ini AND $mes_fin) and anyo = $anyo $filtro_alumno")->queryScalar();
            $total = number_format($suma_total, 2, '.', ',');

            $dataProvider = new SqlDataProvider([
                 'sql' => "SELECT r.id recibo,m.id matricula,a.nombre nombre,a.apellidos apellidos,r.mes mes,r.anyo anyo, r.importe importe  FROM recibos r 
                            JOIN matriculas m  ON r.matricula = m.id join alumnos a  on a.id = m.alumno 
                            where (estado = $tipo) and (mes BETWEEN $mes_ini AND $mes_fin) and anyo = $anyo $filtro_alumno order by mes,apellidos,nombre",
                 'totalCount' => $count,
                'pagination' => false,
//                 'pagination' => ['pageSize' => 15,
//                 ],       
             ]);
              //Yii::$app->request->enableCsrfValidation = false;
            if ($formato == 1){
            
              return $this->render('exportar',
                      ['datos'=>$dataProvider,
                      // 'searchModel' => $searchModel,
                       'model'=>$model,
                       'campos'=>['recibo','matricula','nombre','apellidos','mes','anyo','importe'],
                       'total'=> $total,
                       'mensaje'=>$mensaje,
                      ]);
            }
            
            if ($formato == 2){
                
                return $this->redirect(array_merge(['exportar_excel','mesini'=>$mes_ini,'mesfin'=>$mes_fin,'anyo'=>$anyo,
                              'tipo'=>$tipo,'registros'=>$count,'SumaTotal'=>$suma_total]));       
            }
             if ($formato == 3){
                
                return $this->redirect(array_merge(['exportar_pdf','mesini'=>$mes_ini,'mesfin'=>$mes_fin,'anyo'=>$anyo,
                              'tipo'=>$tipo,'SumaTotal'=>$suma_total]));       
            }
            
        
    }else{
         return $this->render('exportar',['model'=>$model,'mensaje'=>$mensaje]);
    }
    
  }  
    
    public function actionExportar_excel($mesini,$mesfin,$anyo,$tipo,$registros,$SumaTotal){
         
       $itemsmeses =  array(''=>'','1'=>'Enero','2'=>'Febrero','3'=>'Marzo','4'=>'Abril','5'=>'Mayo',
                     '6'=>'Junio','7'=>'Julio','8'=>'Agosto','9'=>'Septiembre','10'=>'Octubre',
                     '11'=>'Noviembre','12'=>'Diciembre');
     
        
        if($tipo == 0){
            $titulo1 = "Recibos Pendientes";
           
        }else if ($tipo == 1){
              $titulo1 = "Recibos Pagados";
             
        }else{
            //todos los recibos. Agrupar pendientes en una hoja y pagados en otra
        }
        
        //si la consulta es de meses distintos se agruparan los resultados en una hoja por cada mes
        if($mesini <> $mesfin){
             $titulo2 = $itemsmeses[$mesini]." - ".$itemsmeses[$mesfin]." / ".$anyo;
        }else{
            $titulo2 = $itemsmeses[$mesini]." / ".$anyo;
        }
        
        $celda = 'E'.($registros + 6);
     
        $query_ = new SqlDataProvider([
                 'sql' => "SELECT a.nombre nombre,a.apellidos apellidos,r.mes mes,r.anyo anyo, r.importe importe FROM recibos r 
                            JOIN matriculas m  ON r.matricula = m.id join alumnos a  on a.id = m.alumno 
                            where (estado = $tipo) and (mes BETWEEN $mesini AND $mesfin) and anyo = $anyo order by mes,apellidos",
                 //'totalCount' => $count,
                'pagination' => false,
//                 'pagination' => ['pageSize' => 15,
//                ],       
             ]);

        $query = $query_->getModels();
  
        
    $file = \Yii::createObject([
        'class' => 'codemix\excelexport\ExcelFile',
        'writerClass' => '\PHPExcel_Writer_Excel2007', // Override default of `\PHPExcel_Writer_Excel2007`
      
        'sheets' => [

            'Listado' => [
                //'class' => 'codemix\excelexport\ActiveExcelSheet',
                  // Name of the excel sheet
            'data' => 
               $query,
            
                'startRow' => 5 ,
                 'on beforeRender' =>function ( $event ) {
//                    $sheet = $event->sender->getSheet();
              
                },
        
////            // If not specified, all attributes from `User::attributes()` are used
//            'attributes' =>
//              ['nombre','apellidos','mes','anyo','importe']
//            ,
                'titles' => ['NOMBRE','APELLIDOS','MES','AÑO','IMPORTE'],
//                    
             'styles' => [
                 'A5:E5' => [
                     'font' => [
                        'bold' =>true ,
                        'color' => [ 'rgb' => '000000' ],
                        
                        'size' => 10 ,
                        'name' => 'Verdana'
                        ],
////                   
//                   'fill' => [
//                            'type' => \PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
//                            'rotation' => 90,
//                            'startcolor' => [
//                                'argb' => 'FFDEAD',
//                            ],
//                            'endcolor' => [
//                                'argb' => 'FFFAF0',
//                            ],
//                        ],
                ],
            ],
//
//            // If not specified, the label from the respective record is used.
//            // You can also override single titles, like here for the above `team.name`
////            'titles' => [
////                'A' => 'Listado de Recibos',
//            ],
        ],

    ],
//                        
    ]);
  
    $file->getWorkbook()->getSheet(0)->getStyle('B1:B3')->getFont()->setSize('15')->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);       
    $file->getWorkbook()->getSheet(0)->getStyle('A1:A3')->getFont()->setSize('15')->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);                
    $file->getWorkbook()->getSheet(0)->getStyle('C1:C3')->getFont()->setSize('15')->setBold('true'); 
    $file->getWorkbook()->getSheet(0)->setCellValue('A1',$titulo1);
    $file->getWorkbook()->getSheet(0)->setCellValue('A2',$titulo2);
    $file->getWorkbook()->getSheet(0)->setCellValue($celda,$SumaTotal);
//    //$file->getWorkbook()->getSheet(0)->setCellValue('B2','Ciclo Formativo');
////     $file->getWorkbook()->getSheet(0)->getStyle('B2')->getFont()->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);
//    //$file->getWorkbook()->getSheet(0)->setCellValue('C2',$nombre_ciclo);
//   // $file->getWorkbook()->getSheet(0)->setCellValue('B3','Denominación');
////     $file->getWorkbook()->getSheet(0)->getStyle('B3')->getFont()->setBold('true')->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_BLUE);
//    //$file->getWorkbook()->getSheet(0)->setCellValue('C3',$denominacion_ciclo);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('A')->setAutoSize(true); 
    $file->getWorkbook()->getSheet(0)->getColumnDimension('B')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('C')->setAutoSize(true);
    $file->getWorkbook()->getSheet(0)->getColumnDimension('D')->setAutoSize(true);

  
    $file->send('informe.xlsx');
  
    }
    
     public function actionExportar_pdf($mesini,$mesfin,$anyo,$tipo,$SumaTotal){
         
         
        $query = new SqlDataProvider([
            'sql' => "SELECT a.nombre nombre,a.apellidos apellidos,r.mes mes,r.anyo anyo, r.importe importe FROM recibos r 
                       JOIN matriculas m  ON r.matricula = m.id join alumnos a  on a.id = m.alumno 
                       where (estado = $tipo) and (mes BETWEEN $mesini AND $mesfin) and anyo = $anyo order by mes,apellidos",

           'pagination' => false,

        ]);
         
        $itemsmeses =  array(''=>'','1'=>'Enero','2'=>'Febrero','3'=>'Marzo','4'=>'Abril','5'=>'Mayo',
                     '6'=>'Junio','7'=>'Julio','8'=>'Agosto','9'=>'Septiembre','10'=>'Octubre',
                     '11'=>'Noviembre','12'=>'Diciembre');
     
        
        if($tipo == 0){
            $titulo1 = "Recibos Pendientes ";
           
        }else if ($tipo == 1){
              $titulo1 = "Recibos Pagados ";
             
        }else{
            //todos los recibos. Agrupar pendientes en una hoja y pagados en otra
        }
        
        //si la consulta es de meses distintos se agruparan los resultados en una hoja por cada mes
        if($mesini <> $mesfin){
             $titulo1 .= $itemsmeses[$mesini]." - ".$itemsmeses[$mesfin]." / ".$anyo;
        }else{
            $titulo1 .= $itemsmeses[$mesini]." / ".$anyo;
        } 
          
          
          
          
          
        $resultado = $query->getModels();

        $mpdf = NEW Mpdf;
        $mpdf->allow_charset_conversion = true;
        $mpdf->charset_in = 'iso-8859-1';
        


       
                    
        $Plantilla = '<!DOCTYPE html>
            <html>
            <head>
                <meta charset="UTF-8">
                <meta name="viewpoort" content="width=device-width,initial-scale=1.0">
                <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
            </head>
            <style>
                main{
                    width:2400px;
                    height:100px;
                }
                header{
                    float:left;
                    margin-left:20px;
                    margin-top:10px;

                    width:95%;
                    height:25%;   
                }

                table th{
                    background-color: #C2CCD1;
                    text-align: left;
                    padding:2px;
                }
                  
                table{
                    width:535px;
                    border:1px black solid;
                    text-align: left;
                    margin-left:10px;
                    border-radius: 10px;
                   

                }
                table tr th td{
                 page-break-inside: avoid !important;
                }

                @media print {
                    footer {
                        page-break-after: always;
                    }
                }


            </style>
            <body>
                <main> 
                <h3>'.$titulo1.
                '</h3><div>
                    <table id="tbl_datos">
                        <tr id="encabezado">
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Mes</th>
                            <th>Año</th>
                            <th>Importe</th>            
                        </tr>';
         foreach ($resultado as $value) {
            $Plantilla .= '<tr>
                            <td>'.$value['nombre'].'</td>
                            <td>'.$value['apellidos'].'</td>
                            <td>'.$value['mes'].'</td>
                            <td>'.$value['anyo'].'</td>
                            <td align="right">'.$value['importe'].'</td>';
        }         
             
        $Plantilla .= '<tr><td></td><td></td><td></td><td>Total</td><td align="right">'.$SumaTotal.'</td></tr>
                    </table>
                </div>
           </main> 
              
            </body>
           
            </html>';
   
        $mpdf->keep_table_proportions = true;
        $mpdf->shrink_tables_to_fit = 1;
        $mpdf->use_kwt = true; 	
        $Plantilla_caract = utf8_decode($Plantilla);
        $Plantilla_utf = $this->EliminarAcentos($Plantilla_caract);
        $mpdf->WriteHTML($Plantilla_utf); 
        $mpdf->Output('informe_recibos.pdf', \Mpdf\Output\Destination::DOWNLOAD);
       exit;
        
        
     }
    
    public function actionEmitir($mesEmi = null,$anyoEmi = null){
    $model = new EmisionRecibos();
    $params = ['mesRecibo'=>$model->mesRecibo,'anyoRecibo'=>$model->anyoRecibo];
    $mensaje="";
     if ($model->load(Yii::$app->request->post())) {
            $mesEmitir = $model->mesRecibo;
            $anyoEmitir = $model->anyoRecibo;
            $fechaEmitir = $model->fechaEmision;
          
        if($model->validarEmision($model->mesRecibo,$model->anyoRecibo)) {
            $errors = $model->errors;
            $mensaje= "Recibos duplicados. No se emitiron.";
            return $this->redirect(["emitir_recibos", "mensaje" => $mensaje]);
            //return $this->redirect("Yii::$app->request->referrer ?: Yii::$app->homeUrl","mensaje"=>$mensaje);
            exit;
        }   
        
            //creamos la consulta con los registros que generan los recibos

                $dataProvider = new SqlDataProvider([
                    'sql' => "SELECT matriculas.id matricula,alumno FROM matriculas 
                               WHERE baja IS NULL or baja = 0 ORDER BY matricula",
	         'pagination' => false        
                ]);



            // bucle que va insertando los registros en recibos con los criterios de seleccion y los emitidos a traves del formulario

                $resultado = $dataProvider->getModels();


                foreach ($resultado as $value) {
                     $matricula = $value['matricula'];
                     $ultimo_importe = new SqlDataProvider([ 'sql' =>"SELECT importe FROM recibos 
                                                             WHERE id =(SELECT MAX(id) FROM recibos
                                                             WHERE matricula = $matricula)",
                                                             
                                                            ]);
                       $importe = $ultimo_importe->getModels();
                       if (empty($importe)){
                            $importe_recibo = 45;
                       }else{
                            foreach ($importe as $value_importe) {
                                $importe_recibo = $value_importe['importe'];  
                            } 
                       }
                       
                        $modelx = new Recibos();
                        $modelx->matricula = $value['matricula'];
                        $modelx->emision = $fechaEmitir;
                        $modelx->mes =  $mesEmitir;
                        $modelx->anyo = $anyoEmitir;
                        $modelx->estado = 0;
                        $modelx->reducido = "";
                        $modelx->importe = $importe_recibo;                  
                        $modelx->save();
                       }
                       
                 return $this->redirect(["consulta_emitidos", "mensaje" => $mensaje,"mesEmi" => $mesEmitir,"anyoEmi" => $anyoEmitir]);       
              
       
        
     }         
} 
    //  controlamos si es una emision nueva o una consulta de recibos emitidos                 
    public function actionConsulta_emitidos($mesEmi = null,$anyoEmi = null){    
       
//       var_dump($_POST);
//        exit;
         $model = new EmisionRecibos();
         
         $searchModel = new RecibosSearch();
         
     
         
        if(!isset($mesEmi)){
            $mesEmitir = $_POST['EmisionRecibos']['mesRecibo'];
            $anyoEmitir = $_POST['EmisionRecibos']['anyoRecibo'];
        }else{
            $mesEmitir = $_GET["mesEmi"];
            $anyoEmitir = $_GET["anyoEmi"];
        }
       
        $parametros=['mes'=>$mesEmitir,"anyo"=>$anyoEmitir];
       
        
                $RecibosEmitidos = new SqlDataProvider([
                     'sql' => "SELECT r.id id, r.matricula matricula,r.emision emision,r.mes mes,r.anyo anyo,r.estado estado,r.reducido reducido,r.importe importe,m.alumno alumno, m.clase clase
                                FROM recibos r join matriculas m on r.matricula = m.id WHERE r.mes = '$mesEmitir' and r.anyo = '$anyoEmitir'",
                     'pagination' => false 
                ]);

              // $resultados = ArrayHelper::toArray($dataRecibosEmitidos);               

    //                   $RecibosEmitidos = ArrayHelper::toArray(Recibos::find()
    //                         ->select('matricula,emision,mes,anyo,estado,reducido,importe')
    //                            ->where(['mes' => $model->mesRecibo])
    //                           ->andWhere("anyo = $model->anyoRecibo")->all());
    //                   echo"<pre>";
    //                   var_dump($RecibosEmitidos);
    //                   echo"</pre>";

    ////                        
                       return $this->render('emitidos',[
                        'model'=>$model,
                        "datos"=>$RecibosEmitidos,
                        "parametros" => $parametros,
                        "searchModel" => $searchModel,   
                        "campos"=>['id','matricula','emision','mes','anyo','estado','reducido','importe','alumno','clase'],
                        "titulo"=>"Emisi�n de Recibos / Matricula",
                        "enunciado"=>"Matriculas a emitir",
                        "sql"=>"SELECT count(*) FROM recibos r join matriculas m on r.matricula = m.id WHERE r.mes = '$mesEmitir' and r.anyo = '$anyoEmitir'",
                    ]);

}   
         
    public function actionFiltrar_recibos(){
         $model = new EmisionRecibos();
         //$modelRecibos = new recibos;
         if ($model->load(Yii::$app->request->post())) {
            $mesEmitir = $model->mesRecibo;
            $anyoEmitir = $model->anyoRecibo;
            $fechaEmitir = $model->fechaEmision;
            $RecibosFiltrados = new SqlDataProvider([
                'sql' => "SELECT r.id id, r.matricula matricula,r.emision emision,r.mes mes,r.anyo anyo,r.estado estado,r.reducido reducido,r.importe importe,m.alumno alumno, m.clase clase
                        FROM recibos r join matriculas m on r.matricula = m.id WHERE (r.mes = '$mesEmitir' and r.anyo = '$anyoEmitir') or emision = '$fechaEmitir'",
            ]);
            return $this->render('emitidos',[
                                'model'=>$model,
                                "datos"=>$RecibosFiltrados,
                                "campos"=>['id','matricula','emision','mes','anyo','estado','reducido','importe','alumno','clase'],
                                "titulo"=>"Consulta de Recibos / Matricula",
                                "enunciado"=>"Recibos a consultar",
                                "sql"=>"SELECT count(*) FROM recibos r join matriculas m on r.matricula = m.id WHERE r.mes = '$mesEmitir' and r.anyo = '$anyoEmitir'",
                            ]);
            
         }
        
    }
        
    public function actionRecibospdf($matricula=Null,$mes=Null,$anyo=Null){
//        $Recibospdf = new SqlDataProvider([
//            'sql' => "SELECT r.id recibo, r.matricula matricula,r.emision emision,r.mes mes,r.anyo anyo,r.estado estado,r.reducido reducido,r.importe importe,m.alumno alumno, m.clase clase
//                       FROM recibos r join matriculas m on r.matricula = m.id WHERE r.mes = 12 and r.anyo = 2019",
//        ]);
        
         $model = new EmisionRecibos();
         $modelRecibos = new recibos;
         if ($model->load(Yii::$app->request->post())) {
            $mesEmitir = $model->mesRecibo;
            $anyoEmitir = $model->anyoRecibo;
            $fechaEmitir = $model->fechaEmision;
            $filtro_query = "";
         }else{
            $mesEmitir = $mes;
            $anyoEmitir = $anyo;
            $fechaEmitir = "1-".$mes."-".$anyo;
            $filtro_query = " And mat.id = $matricula";
         }    
//            $RecibosFiltrados = new SqlDataProvider([
//                'sql' => " SELECT r.id recibo, r.matricula matricula,r.emision emision,r.mes mes,r.anyo anyo,r.estado estado,r.reducido reducido,r.importe importe,m.alumno alumno, m.clase clase,al.nombre nombre,al.apellidos apellidos,al.direccion direccion,al.dni dni,al.movil movil,cur.curso curso,asigna.asignatura asignatura
//                       FROM recibos r join matriculas m on r.matricula = m.id JOIN alumnos al ON m.alumno = al.id  JOIN clases cla ON cla.id = m.clase  JOIN cursos cur ON cur.id = cla.curso JOIN asignaturas asigna ON asigna.id = cla.asignatura
//                       WHERE (r.mes = '$mesEmitir' and r.anyo = '$anyoEmitir') or emision = '$fechaEmitir'",
////                'sql' => "SELECT r.id recibo, r.matricula matricula,r.emision emision,r.mes mes,r.anyo anyo,r.estado estado,r.reducido reducido,r.importe importe,m.alumno alumno, m.clase clase
////                        FROM recibos r join matriculas m on r.matricula = m.id WHERE (r.mes = '$mesEmitir' and r.anyo = '$anyoEmitir') or emision = '$fechaEmitir'",
//            ]);
            
            $alumnos_matricula_activa = new SqlDataProvider([
               'sql'=>"SELECT DISTINCT al.id alumno,al.nombre nombre,al.apellidos apellidos,al.dni dni,al.direccion direccion,al.poblacion poblacion,al.movil movil,centro.nombre centro
                                FROM alumnos al 
                                    JOIN centros centro on al.centro = centro.id
                                    JOIN matriculas mat ON al.id = mat.alumno 
                                    JOIN recibos rec 
                                        WHERE mat.baja IS NULL 
                                            AND mat.alumno 
                                                NOT IN(SELECT alumno_grupo FROM agrupan) 

                                                    AND rec.mes = $mesEmitir
                                                     AND rec.anyo = $anyoEmitir
                                                         $filtro_query

 ORDER BY mat.id ASC",                                                       
                  'pagination'=>false 
            ]);
            
            
            
            $recibos_alumno = new SqlDataProvider([
                'sql'=>"SELECT r.id recibo, mat.id matricula,r.emision fechemi,r.mes mes,r.anyo anyo,r.importe importe,mat.alumno, cur.curso curso, asigna.asignatura asignatura 
                                    FROM recibos r JOIN matriculas mat ON mat.id = r.matricula
                                        JOIN clases clas ON mat.clase = clas.id 
                                        JOIN cursos cur ON clas.curso = cur.id 
                                        JOIN asignaturas asigna ON clas.asignatura = asigna.id 
                                            WHERE baja IS NULL
                                                AND r.mes = $mesEmitir
                                                AND r.anyo = $anyoEmitir",

 'pagination'=>false 
            ]);
            
            $recibos_alumnoAgrupado= new SqlDataProvider([
              'sql'=>"SELECT r.id recibo, mat.id matricula,r.emision fechemi,r.mes mes,r.anyo anyo,r.importe importe,agrup.alumno alumno, cur.curso curso, asigna.asignatura asignatura 
                            FROM recibos r 
                                JOIN matriculas mat ON mat.id = r.matricula
                                JOIN clases clas ON mat.clase = clas.id JOIN cursos cur ON clas.curso = cur.id JOIN asignaturas asigna ON clas.asignatura = asigna.id
                                JOIN agrupan agrup ON mat.alumno = agrup.alumno_grupo
                                    WHERE baja IS NULL
                                        AND r.mes = $mesEmitir
                                        AND r.anyo = $anyoEmitir 
                                            ORDER BY recibo", 
                 'pagination'=>false 
            ]);
            
            $recibos_alumnoAgrupadoMaterial = new SqlDataProvider([
                'sql'=>"SELECT a.id alumno,SUM(importe)totalmaterialAgr FROM alumnos a RIGHT JOIN agrupan ON alumno = a.id 
                        JOIN material ON (a.id = material.alumno 
                                  OR alumno_grupo = material.alumno)
                                    WHERE MONTH(fecha) = $mesEmitir 
                                            AND year(fecha) = $anyoEmitir 
                                             GROUP BY a.id",
                 'pagination'=>false 
            ]);
            
             $recibos_alumnoNoAgrupadoMaterial = new SqlDataProvider([
                'sql'=>"SELECT m.alumno alumno, SUM(importe)totalmaterialNoagr FROM material m
                        WHERE MONTH(fecha) = $mesEmitir 
                            AND year(fecha) = $anyoEmitir 
                            AND m.alumno NOT IN(SELECT DISTINCT alumno FROM agrupan)
                            AND m.alumno NOT IN(SELECT DISTINCT alumno_grupo FROM agrupan)
                              GROUP BY m.alumno",
                  'pagination'=>false 
            ]);
            
            $recibos_alumnoPendientes =  new SqlDataProvider([
                'sql'=>"SELECT mat.alumno alumno,rec.id recibo, rec.mes mes, rec.anyo, rec.emision, rec.importe
                         FROM recibos rec 
                            JOIN matriculas mat ON mat.id = rec.matricula 
                                WHERE rec.estado = FALSE 
                                and rec.mes <> $mesEmitir
                                and rec.anyo <= $anyoEmitir",
                 'pagination'=>false 
            ]);
            
        
        $resultado = $alumnos_matricula_activa->getModels();
        $recibos_activos = $recibos_alumno->getModels();
        $recibos_agrupados = $recibos_alumnoAgrupado->getModels();
        $recibos_material_agrupado = $recibos_alumnoAgrupadoMaterial->getModels();
        $recibos_material_noagrupado = $recibos_alumnoNoAgrupadoMaterial->getModels();
        $recibos_pendientes = $recibos_alumnoPendientes->getModels();
        $meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        setlocale(LC_TIME, "spanish");
          
       
                $mpdf = NEW Mpdf;
				$mpdf->allow_charset_conversion = true;
				$mpdf->charset_in = 'iso-8859-1';
             
                
                foreach ($resultado as $value) {
                    $total = 0;
                     $swAlumnoMaterial = 0; //switch que controla si un alumno tiene material de fotocopias o no
                    
                $reciboPlantilla= '<!DOCTYPE html>
                    <html>
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewpoort" content="width=device-width,initial-scale=1.0">
                        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
                    </head>
                    <style>
                        main{
                            width:2400px;
                            height:100px;
                        }
                        header{
                            float:left;
                            margin-left:20px;
                            margin-top:10px;
                          
                            width:95%;
                            height:25%;   
                        }
                        header .cab1,header .cab2,header .cab3{
                            width: 30%;
                            height: 50px;
                           
                            float:left;
                            margin-left:22px;
                            margin-top:2px;
                        }
                        header .cab1{
                            background-image: url("../data/imagenes/LogoAcademiaNoelia.jpg");
                            background-repeat: no-repeat;
                            background-size: 80%;  
                        }
                        header .cab2{
                            width:25%;
                            font-size: 8px;
                            text-align: center;
                            position:relative;
                            top:10px;
                        }   
                        header .cab3{
                            width: 32%;
                            font-size: 15px;
                            text-align: right;
                            line-height: 150px;
                        }  
                        section{
                           
                            margin-left:20px;
                            margin-top:1px;
                            width:95%;
                          
                        }
                      
                        section.datos_observa{
                            margin-bottom:10px;
                        }
                        div.personales{
                            float:left;
                            width:60%;
                            padding:10px;
                            margin-top:-170px;
                            border:1px solid #220044;
                            background-color: #f0f2ff;
                            border-style:solid;
                            border-radius: 15px;
          
                        }
                        div.pago{
                            float:left;
                            width:30%;
                            height:30px;
                            margin-left:10px;
                            padding:10px;
                            border:1px solid #220044;
                            background-color: #f0f2ff;
                            border-style:solid;
                            border-radius: 15px;

                        }
                        div.personales label, div.pago label{
                            background-color:#C2CCD1;
                        }

                        table th{
                            background-color: #C2CCD1;
                            text-align: left;
                            padding:2px;
                        }
                        table#tbl_datos{
                            width:700px;
                           
                        }
                        table#tbl_pago{
                            width:330px;
                            border:1px black solid;
                            margin-left:15px;
                            height:76px;
                            text-align: center;
                            border-radius: 10px;
                        }
                        table#tbl_pago th{
                            text-align: center;
                        }
                         div.concepto{
                            border:1px solid #220044;
                            background-color: #f0f2ff;
                            border-radius: 15px;
                           
                         }
                         table#tbl_concepto{
                            width:935px;
                            border:1px black solid;
                            text-align: center;
                            margin-left:10px;
                            border-radius: 10px;
                          
                        }
                        table#tbl_concepto th{
                            text-align: center; 
                            
                        }
                        div.total{
                            border:1px solid #220044;
                            background-color: #f0f2ff;
                            border-radius: 15px;
                        
                        }
                        table#tbl_observaciones{
                            width:935px;
                            border:1px black solid;
                            text-align: center;
                            margin-left:10px;
                            border-radius: 10px;
                          
                        }
                        table#tbl_observaciones th{
                            text-align: center; 
                            
                        }
                         div.observaciones{
                            border:1px solid #220044;
                            background-color: #f0f2ff;
                            border-radius: 15px;
                            text-align:left;
                        }
                        table#tbl_totales{
                            width:935px;
                            border:1px black solid;
                            text-align: center;
                            margin-left:10px;
                            border-radius: 10px;
                            
                        }
                        table#tbl_totales th{
                            text-align: center;
                            width:720px;
                        }
                        .contenedor_central{
                            margin-top:-100px;
                        }
                        @media print {
                            footer {
                                page-break-after: always;
                            }
                        }
                        

                    </style>
                    <body>
                        <main>     

                            <header>
                                <div class="cab1">

                                </div>
                                <div class="cab2">
                                    <strong>CENTRO DE ESTUDIOS NOELIA PÈREZ</strong><br/>
                                    <span>Residencial Monte Castillo</span><br/>
                                    <span>Avda. de Oviedo, 20 E-F bajos</span><br/>
                                    <span>39710 SOLARES (Cantabria)</span><br/>
                                    <span>Telf.: 619 518 491</span><br/>
                                    <span>academianoeliaperez@gmail.com</span>
                                </div>
                                
                            </header>
                            <section class="datos_alumno">
                                <div class="personales">
                                        <table id="tbl_datos">
                                            <tr colspan="2">
                                                <th>Alumno</th>
                                                <td>'.$value['nombre'].' '.$value['apellidos'].'</td>   
                                            </tr>
                                            <tr colspan="2">
                                                <th>Centro</th><td>'.$value['centro'].'</td>
                                               
                                            </tr>    
                                            <tr colspan="2">
                                                <th>Dirección</th><td style="margin-right:35px;">'.$value['direccion'].'</td>
                                                 <th>Localidad</th><td>'.$value['poblacion'].'</td>
                                            </tr>


                                        </table>
                                </div>
                                <div class="pago">
                                    <table id="tbl_pago">
                                            <tr colspan="4">
                                                <th>Fecha</th>
                                              
                                                <th>Período</th>

                                            </tr>
                                            <tr colspan="4">
                                                <td>'.$fechaEmitir.'</td>
                                              
                                                <td>'.ucfirst(strftime("%B", strtotime($fechaEmitir))).'/'.$anyoEmitir.'</td>

                                            </tr> 
                                    </table>
                                </div>
                            </section>
                         <div class="contenedor_central">   
                            <section class="datos_clase">
                              <div class="concepto">
                                <table id="tbl_concepto">
                                    <tr colspan="2">
                                        <th>Recibo</th>
                                        <th>Concepto</th>
                                        <th>Importe</th>
                                    </tr>';
                               
                                    foreach ($recibos_activos as $activos) {
                                        if($value['alumno'] == $activos['alumno'] ){
                                         $reciboPlantilla .= '<tr colspan="2">
                                                <td>'.$activos['recibo'].'</td>
                                                <td>'.$activos['curso']. ' - '.$activos['asignatura'].'</td>
                                                <td></td>
                                            </tr>'; 
                                         $total += $activos['importe'];
                                        }
                                     }
                                    foreach ($recibos_agrupados as $agrupados) {
                                        if($value['alumno'] == $agrupados['alumno'] ){
                                         $reciboPlantilla .= '<tr colspan="2">
                                                <td>'.$agrupados['recibo'].'</td>
                                                <td>'.$agrupados['curso']. ' - '.$agrupados['asignatura'].'</td>
                                                <td></td>
                                            </tr>';   
                                          $total += $agrupados['importe'];
                                        }
                                     }
                                     foreach ($recibos_material_agrupado as $material) {
                                        if($value['alumno'] == $material['alumno'] ){
                                         $reciboPlantilla .= '<tr colspan="2">
                                                <td>material</td>
                                                <td>Fotocopias</td>
                                                <td>'.$material['totalmaterialAgr'].'</td>
                                            </tr>';  
                                        $total += $material['totalmaterialAgr'];
                                        $swAlumnoMaterial = 1;
                                        }
                                     }
                                    foreach ($recibos_material_noagrupado as $materialNoAgr) {
                                        if($value['alumno'] == $materialNoAgr['alumno'] ){
                                         $reciboPlantilla .= '<tr colspan="2">
                                                <td>material</td>
                                                <td>Fotocopias</td>
                                                <td>'.$materialNoAgr['totalmaterialNoagr'].'</td>
                                            </tr>';  
                                        $total += $materialNoAgr['totalmaterialNoagr'];
                                         $swAlumnoMaterial = 1;
                                        }
                                     }
                                     
                              
                                     
                                     if($swAlumnoMaterial == 0){
                                        $reciboPlantilla .= '<tr colspan="2">
                                            <td>material</td>
                                            <td>Fotocopias</td>
                                            <td>0</td>
                                        </tr>';   
                                         //$swAlumnoMaterial = 0; //switch que controla si un alumno tiene material de fotocopias o no
                                     }
                                    
                            $reciboPlantilla.= '</table>
                              </div>
                            </section>
                            <section class="totales">
                                <div class="total">
                                    <table id="tbl_totales">
                                        <tr colspan="2">
                                            <th>Total</th><td>'.number_format($total, 2, '.', '').'</td>
                                        </tr>
                                    </table>
                                </div>
                            </section>
                            <section class="datos_observa">
                              <div class="observaciones">
                                <table id="tbl_observaciones">
                                   
                                
                                    <tr colspan="1">
                                        <th>Observaciones</th>
                                    </tr>
                                    <tr colspan="3">
                                        <td>Recibos Pendientes:</td>';
                                        foreach ($recibos_pendientes as $pendientes) {
                                           if($value['alumno'] == $pendientes['alumno'] ){
                                            $reciboPlantilla .=                                            
                                                   '<td>'.$meses[$pendientes['mes']-1].'-'.$pendientes['anyo'].' / '.$pendientes['importe'].'</td>
                                               </tr>';  
       
                                           }
                                        };     
                                            
         
            $reciboPlantilla .= '</table>
                              </div>
                            </section>
                         
                            
                   </footer>
                        </main> 
                        <hr>  
                    </body>
                    </div>
                    </html>';
//                    
//                    $reciboPlantilla = '<div class="recibos-index">
//                                    <section class="cabecera" style="width:700px;height:100px">  
//                                        <div class="logo" style="float:left;width:33%">
//                                            <img src="../data/imagenes/logo.png"/>  
//                                        </div>
//        
//       
//                                        <div class="datosAca" style="float:left;width:33%;text-align:center;line-height:1px;font-size:10px;font-famili:arial;">
//                                            <p><b>Academia Noelia P�rez</b></p>
//                                            <p>Avenida de Oviedo, 20 bajos</p>
//                                            <p>Solares</p>
//                                            <p>(+34) 619 51 84 91</p>
//                                            <p>academianoeliaperez@gmail.com</p>
//                                            
//                                        </div>
//                                        <div class="recibonum" style="float:left;width:33%;padding-top:100px;text-align:right">
//                                            <span>Recibo N�'.$value['recibo'].'</span>
//                                        </div>
//                                    </section>
//                                    <section class="cabecera1" style="width:700px;height:75px;float:left;clear:both">
//                                        <div style="float:left;width:400px;">
//                                            <table style="border:solid 1px #b3b7bb;border-radius:35px;widht:100%;">
//                                                <thead style="float:left">
//                                                  <tr>
//                                                    <th style="display:block;background-color:#b3b7bb;font-size:12px">Recibimos de</th>
//                                                    <td style="float:right;font-size:12px;">'.$value['apellidos'].' ' .$value['nombre'].'</td>
//                                                  </tr>
//                                                  <tr>
//                                                    <th style="display:block;background-color:#b3b7bb;font-size:12px">Direcci�n</th>
//                                                    <td style="float:right;font-size:12px;">'.$value['direccion'].'</td>
//                                                  </tr>
//                                                </thead>
//                                               
//                                            </table>
//                                        </div>
//                                        <div style="float:right;width:200px;">
//                                            <table style="border:solid 1px #b3b7bb;border-radius:35px;widht:100%;">
//                                                <thead style="float:left">
//                                                  <tr>
//                                                    <th style="background-color:#b3b7bb;font-size:12px">Fecha Recibo</th><th style="background-color:#b3b7bb;font-size:12px">Medio de Pago</th>
//                                                  </tr>
//                                                  <tr>
//                                                        <td style="float:right;font-size:12px">'.$value['emision'].'</td>
//                                                        <td style="float:right;font-size:12px">Efectivo</td>
//                                                  </tr>
//                                            </table>
//                                        </div>
//                                        <div>
//                                        </div>
//                                        
//                                    </section>
//                                    
//                                    <section class="cabecera2" style="width:1000px;height:30px;float:left;clear:both">
//                                        <div style="float:left;width:700px;">
//                                            <table width="1000px" height="200px" style="border:solid 1px grey;border-radius:35px;">
//                                                <thead style="float:left">
//                                                  <tr>
//                                                    <th style="display:block;background-color:#b3b7bb;font-size:15px;display:block;width:200px">El valor de</th>
//                                                    <td style="float:left;font-size:12px;width:800px;">'.$value['importe'].'</td>
//                                                  </tr>
//                                                </thead>   
//                                            </table>
//                                        </div>    
//                                    </section>
//                                    <section class="cabecera3" style="width:1000px;height:70px;float:left;clear:both">
//                                        <div style="float:left;width:1000px;">
//                                            <table width="1000px" height="300px" style="border:solid 1px grey;border-radius:35px;">
//                                                <thead style="float:left">
//                                                    <tr>
//                                                        <th style="background-color:#b3b7bb;font-size:15px">Concepto</th>
//                                                    </tr>   
//                                                    <tr>
//                                                        <td style="font-size:12px;">'.$value['curso'].' / '.$value['asignatura'].'</td>
//                                                    </tr>
//                                                </thead>   
//                                            </table>
//                                        </div>    
//                                    </section>
//
//                                            
//                                </div>';
                    
//                    $recibo = "<div class='container'>
//                                <table style='border:solid;width:800px;'>
//                                    <tr>
//                                        <th>RECIBO N�</th> <th>LOCALIDAD DE EXPEDICI�N</th> <th>IMPORTE</th>
//                                    </tr>
//                                    <tr>
//                                        <td style='text-align:center;'>".$value['recibo']."<td/> <td style='text-align:center;'>Solares</td><td style='text-align:center;'>".$value['importe']."</td>
//                                    </tr>
//                                    <tr>
//                                     <th>FECHA DE EXPEDICI�N</th> <th>VENCIMIENTO</th>
//                                    </tr>
//                                    <tr>
//                                        <td style='text-align:center;'>".$value['emision']."<td/><td style='text-align:center;'>01-01-2020</td>
//                                    </tr>
//                                </table>   
//                            </div><hr>";
                       $mpdf->keep_table_proportions = true;
                       $mpdf->shrink_tables_to_fit = 1;
                       $mpdf->use_kwt = true; 
					   //$reciboPlantilla_convert_utf8 = mb_convert_encoding($reciboPlantilla, 'UTF-8', 'UTF-8');
 					   $recibo = utf8_decode($reciboPlantilla);
                       $recibo_utf = $this->EliminarAcentos($recibo);

                       $mpdf->WriteHTML($recibo_utf); 
                    }
 
      
       
        $mpdf->Output();
        exit;
       
    }
         
         
       public function EliminarAcentos($cadena){
		
        //Reemplazamos la A y a
        $cadena = str_replace(
        array('�', '�', '�', '�', '�', '�', '�', '�', '�'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
        $cadena
        );

        //Reemplazamos la E y e
        $cadena = str_replace(
        array('�', '�', '�', '�', '�', '�', '�', '�'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
        $cadena );

        //Reemplazamos la I y i
        $cadena = str_replace(
        array('�', '�', '�', '�', '�', '�', '�', '�'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
        $cadena );

        //Reemplazamos la O y o
        $cadena = str_replace(
        array('�', '�', '�', '�', '�', '�', '�', '�'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
        $cadena );

        //Reemplazamos la U y u
        $cadena = str_replace(
        array('�', '�', '�', '�', '�', '�', '�', '�'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
        $cadena );

        //Reemplazamos la N, n, C y c
        $cadena = str_replace(
        array('�', '�', '�', '�'),
        array('N', 'n', 'C', 'c'),
        $cadena
        );
         //Reemplazamos la �
        $cadena = str_replace(
        array('º'),
        array('�',),
        $cadena
        );
       

        return $cadena;
    }

   






    }
 
