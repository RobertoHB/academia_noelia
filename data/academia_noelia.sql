-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-11-2020 a las 00:02:27
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `academia_noelia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agrupan`
--

CREATE TABLE `agrupan` (
  `id` int(11) NOT NULL,
  `alumno` int(11) DEFAULT NULL,
  `alumno_grupo` int(11) DEFAULT NULL,
  `observaciones` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `agrupan`
--

INSERT INTO `agrupan` (`id`, `alumno`, `alumno_grupo`, `observaciones`) VALUES
(1, 15, 16, ''),
(2, 11, 82, ''),
(3, 78, 55, ''),
(4, 56, 23, ''),
(5, 116, 114, ''),
(6, 17, 52, ''),
(7, 61, 41, ''),
(8, 83, 9, ''),
(9, 28, 71, ''),
(10, 25, 88, ''),
(11, 94, 77, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE `alumnos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(200) DEFAULT NULL,
  `dni` varchar(9) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `poblacion` varchar(100) DEFAULT NULL,
  `movil` int(11) DEFAULT NULL,
  `fijo` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `alta` date DEFAULT NULL,
  `curso` varchar(50) DEFAULT NULL,
  `centro` int(11) DEFAULT NULL,
  `observaciones` text DEFAULT NULL,
  `pago` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`id`, `nombre`, `apellidos`, `dni`, `direccion`, `poblacion`, `movil`, `fijo`, `email`, `alta`, `curso`, `centro`, `observaciones`, `pago`) VALUES
(1, 'Marina ', 'Soto Lavín', '', 'C/ Calvo Sotelo, 1, 3º izq. ', 'Solares (Medio Cudeyo)', 617342420, NULL, '', '2020-10-01', '9', 3, '', '1'),
(5, 'Alba', 'Perdiguero Sánchez', '', 'Bº La Sierra, 52 bj. C', 'Hoznayo (Entrambasaguas)', 666960835, NULL, '', '2020-10-01', '9', 3, '', '1'),
(7, 'Claudia', 'Rodríguez', '', 'Bº Padierne, urb. La Mies', 'Navajeda (Entrambasaguas)', 666242576, NULL, '', '2020-10-01', '9', 3, '', '1'),
(8, 'Sonia', 'Sierra Corral', '', 'Bº La Llama, 41', 'Gajano (Marina de Cudeyo)', 655645754, NULL, '', '2020-10-01', '10', 2, '', '1'),
(9, 'Martín ', 'López Miranda', '', 'C/ Los Morriones, 3', 'Sobarzo (Penagos)', 619743524, NULL, '', '2020-10-01', '10', 11, '', '1'),
(10, 'Santiago', 'Alonso Urueña ', '', 'Avda. Oviedo, 20 B, 5º B', 'Solares (Medio Cudeyo)', 618650343, NULL, '', '2020-10-01', '10', 1, '', '1'),
(11, 'Martín ', 'Eghaghe', '', 'C/ Emilio Maza Cifrián', 'Solares (Medio Cudeyo)', 646803292, NULL, '', '2020-10-01', '10', 1, '', '1'),
(12, 'Alejandro', 'Riera', '', 'Bº Rioz, 141', 'Sobremazas (Medio Cudeyo)', 611461205, NULL, '', '2020-10-01', '10', 1, '', '1'),
(13, 'Alonso', 'Santamaría Setién', '', 'Bº La iglesia, 37', 'Entrambasaguas', 615583312, NULL, '', '2020-10-01', '10', 1, '', '1'),
(14, 'Hugo', 'Arnáiz Cruz', '', 'Avda. Oviedo, 20 B, 3º B', 'Solares (Medio Cudeyo)', 662913246, NULL, '', '2020-10-01', '10', 3, '', '1'),
(15, 'Pablo', 'Basanta Galarón', '', 'C/ La Llamosa, 28', 'Galizano (Ribamontán al Mar)', 629365941, NULL, '', '2020-10-01', '10', 3, '', '1'),
(16, 'Irene', 'Basanta Galarón', '', 'C/ La Llamosa, 45', 'Galizano (Ribamontán al Mar)', 629365941, NULL, '', '1970-01-01', '10', 3, '', '1'),
(17, 'Carla', 'Cobo Martín', '', 'Bº Mercadillo', 'Liérganes', 696586484, NULL, '', '2020-10-01', '10', 3, '', '1'),
(18, 'Irma', 'Rodríguez Fages', '', 'Paseo Virgen del Pilar, 14', 'Solares (Medio Cudeyo)', 627896446, NULL, '', '2020-10-01', '11', 1, '', '1'),
(20, 'Catalin', 'Popanu', '', 'La Rañada, 12 B', 'Entrambasaguas', 642950501, NULL, '', '2020-10-01', '11', 1, '', '1'),
(21, 'Héctor', 'Costas del Campo', '', 'Bº Elechino, 28', 'Entrambasaguas', 610266889, NULL, '', '2020-10-01', '11', 1, '', '1'),
(22, 'Pablo ', 'Ruiz Ordiérez', '', 'Avda. Oviedo, 20 A 1º A', 'Solares (Medio Cudeyo)', 603403268, NULL, '', '2020-10-01', '11', 1, '', '1'),
(23, 'Carolina ', 'Pila', '', 'Bº Trillogua, 178', 'Ceceñas (Medio Cudeyo)', 626817251, NULL, '', '2020-10-01', '11', 1, '', '1'),
(24, 'Marey', 'Moreira Enríquez', '', 'Bº Padierne, 20 Navajeda', 'Entrambasaguas', 663215021, NULL, '', '2020-10-01', '11', 1, '', '1'),
(25, 'Elena ', 'Álvarez Barquinero', '', 'Solares', 'Medio Cudeyo', 629806518, NULL, '', '2020-10-01', '11', 2, '', '1'),
(26, 'Alba', 'Cobo Ruiz', '', 'Bº Cotornal, 178', 'Ánaz (Medio Cudeyo)', 699124326, NULL, '', '2020-10-01', '11', 2, '', '1'),
(27, 'Desirée ', 'López Muñoz', '77', 'Bº Vallas, 6 ', 'Setién (Mariana de Cudeyo)', 657743530, NULL, '', '2020-10-01', '11', 2, '', '1'),
(28, 'Bousso', 'Tene', '', 'Bº La Sota', 'Anero (Ribamontán al Monte)', 643265956, NULL, '', '2020-10-01', '11', 2, '', '1'),
(29, 'Alba', 'López Iglesias', '', 'Bº Fuente la Plata, 86', 'Ceceñas (Medio Cudeyo)', 609871019, NULL, '', '2020-10-01', '12', 1, '', '1'),
(30, 'Olimpia ', 'Cobo Renedo', '', 'Bº Buspombo, 6', 'Liérganes', 689468977, NULL, '', '2020-10-01', '12', 1, '', '1'),
(31, 'Nacho', 'Escobedo Corral', '', 'Bº Las Cagigas, 52', 'Heras (Medio Cudeyo)', 640524274, 942527321, '', '2020-10-01', '12', 2, '', '1'),
(32, 'David ', 'Viar Gutiérrez', '', 'Bº La Iglesia, 26', 'Entrambasaguas', 626321122, NULL, '', '2020-10-01', '12', 1, '', '1'),
(33, 'David', 'Laso Berasategui', '', 'Bº Ontañón', 'Helechas (Marina de Cudeyo)', 630260095, NULL, '', '2020-10-01', '12', 2, '', '1'),
(34, 'Aymar', 'García Cerezal', '', 'Bº Las Lastra ', 'Riaño (Entrambasaguas)', 630627217, NULL, '', '2020-10-01', '12', 1, '', '1'),
(35, 'Laura ', 'Sierra Basaldua ', '', 'Bº Rioz, 102', 'Sobremazas (Medio Cudeyo)', 659196781, NULL, '', '2020-10-01', '12', 1, '', '1'),
(37, 'Fátima ', 'Blanco Ríos', '', 'Bº El Cagigal, 10', 'Hoz de Anero (Ribamontán al Monte)', 626581992, NULL, '', '2020-10-01', '11', 3, '', '1'),
(38, 'Lorena', 'Cañizo Alonso', '', 'C/ Deán Martínez Mazas, 29', 'Liérganes ', 617040761, NULL, '', '2020-10-01', '13', 18, '', '1'),
(39, 'Cristian', ' Matanza Cabarga', '', 'La Rañada, 14', 'Entrambasaguas', 663633332, NULL, '', '2020-10-01', '13', 9, '', '1'),
(41, 'Pablo ', 'Bolívar Cagigal', '', 'C/ Constitución, 5, 5º izq. ', '', 637988230, NULL, '', '2020-10-01', '8', 1, '', '1'),
(42, 'Hugo ', 'Cortázar Lavín', '', 'Solares', 'Medio Cudeyo', NULL, NULL, '', '2020-10-01', '5', 7, '', '1'),
(43, 'Ana ', 'Cuesta', '', 'Bº La Tejera, 3', 'Entrambasaguas', NULL, NULL, '', '2020-10-01', '10', 1, '', '1'),
(44, 'Daniel', 'Gómez Gómez', '', 'La Rañada, 14', ' El Bosque (Entrambasaguas)', 645088619, NULL, '', '2020-10-01', '9', 7, '', '1'),
(45, 'Rodrigo', 'García González de la Higuera ', '', 'Bº de la Gándara 121', '', 664599605, NULL, '', '2020-10-01', '12', 1, '', '1'),
(46, 'María ', 'Gándara Colina', '', 'Bº El Sol, 2, 2º B', 'Villaverde de Pontones', 625393591, NULL, '', '2020-10-01', '12', 2, '', '1'),
(47, 'Ricardo', 'Alonso Rojas', '', 'Avda. Oviedo, 18, 4º A', 'Solares (Medio Cudeyo)', 654321544, NULL, '', '2020-10-01', '11', 1, '', '1'),
(48, 'Hugo', 'García Samperio', '', 'Avda. Oviedo, 20 I, 3º C', 'Solares (Medio Cudeyo)', 616390791, NULL, '', '2020-10-01', '12', 1, '', '1'),
(49, 'Andrea', 'Calvo Badiola', '', 'Bº La Sierra, 57', 'Hoznayo (Entrambasaguas)', 654071328, NULL, '', '2020-10-01', '10', 1, '', '1'),
(50, 'Cayetana', 'Bravo Ranero', '', 'Bº Monasterio, 3', 'Escobedo de Camargo', 618277111, NULL, '', '2020-10-01', '8', 3, '', '1'),
(51, 'Ana Lucía', 'Higuera del Monte', '', 'Bº LA Iglesia, 101', 'Cubas (Ribamontán al Monte)', 635440498, NULL, '', '2020-10-01', '8', 3, '', '1'),
(52, 'Sara', 'Cobo Martín', '', 'C/ Mercadillo', 'Liérganes', 661545305, NULL, '', '2020-10-01', '8', 3, '', '1'),
(53, 'Darío ', 'Sánchez Díaz', '', '', 'Navajeda (Entrambasaguas)', 680361341, NULL, '', '1970-01-01', '8', 2, '', '1'),
(54, 'Manuel', 'Mesa Merino', '', 'La Rañada', 'El Bosque (Entrambasaguas)', 615378630, NULL, '', '2020-10-01', '7', 1, '', '1'),
(55, 'Adrián', 'Blanco Sañudo', '', 'C/ El Crucero', 'Solares (Medio Cudeyo)', 634474327, NULL, '', '2020-10-01', '8', 1, '', '1'),
(56, 'Adrián ', 'Pila', '', 'Bº Trillogua, 178', 'Ceceñas (Medio Cudeyo)', 600810985, NULL, '', '2020-10-01', '8', 1, '', '1'),
(57, 'Carmen', 'González Ruiz', '', 'Solares', 'Medio Cudeyo', 696184079, NULL, '', '2020-10-01', '8', 1, '', '1'),
(58, 'Sergio', 'Martín Trueba', '', 'Solares', 'Medio Cudeyo', 629003990, 722692746, '', '2020-10-01', '7', 3, '', '1'),
(59, 'Víctor', 'Pandiello Pérez', '', 'Bª Alto del Bosque, 8', 'Entrambasaguas', 622343080, NULL, '', '2020-10-01', '10', 1, '', '1'),
(60, 'Jairo ', 'Fernández Salazar ', '', 'Solares', 'Medio Cudeyo', 601251012, NULL, '', '2020-10-01', '10', 1, '', '1'),
(61, 'Ariana', 'Bolívar Cagigal', '', 'C/ Constitución, 5, 5º izq. ', 'Solares (Medio Cudeyo)', 680911560, NULL, '', '2020-10-01', '11', 1, '', '1'),
(62, 'Alba ', 'Fernández Bedoya', '', 'Avda. Oviedo, 10 3º izq. ', 'Solares Medio Cudeyo', 603446626, NULL, '', '2020-10-01', '12', 3, '', '1'),
(63, 'Diego', 'Acebo Cobo', '', 'Urb. Río Miera, 4 1 C', 'Solares Medio Cudeyo', 667037661, NULL, '', '2020-10-01', '10', 1, '', '1'),
(64, 'Claudia', 'Gómez Herrera', '', 'C/ Centro, 38', 'La Cavada Riotuerto', NULL, NULL, '', '2020-10-01', '11', 1, '', '1'),
(65, 'Laura ', 'Cano Hazas', '', 'Avda. Oviedo, 20 H', 'Solares Medio Cudeyo', 638567944, NULL, '', '2020-10-01', '11', 1, '', '1'),
(66, 'Verónica', 'Fernández Lavín', '', 'San Roque de Riomiera', '', 622024436, NULL, '', '2020-10-01', '11', 1, '', '1'),
(67, 'Ainara', 'Llata Tobar', '', 'Bº La Rañada, 10 F, 2 A', 'Entrambasaguas', 656385859, NULL, '', '2020-10-01', '9', 13, '', '1'),
(68, 'Paula', 'Perales Sierra', '', 'Avd. Oviedo, 20 E, 1º A', 'Solares Medio Cudeyo', 676214428, NULL, '', '2020-10-01', '10', 1, '', '1'),
(69, 'Óscar ', 'Sarabia Cobo', '', 'C/ Río Miera 6, 4º B ', 'Solares Medio Cudeyo', 675272042, NULL, '', '2020-10-01', '3', 14, '', '1'),
(70, 'Nicolás ', 'Lanza ', '', 'Heras', 'Medio Cudeyo', 684174897, NULL, '', '2020-10-01', '12', 2, '', '1'),
(71, 'Falou', 'Tene', '', 'Bº La Sota, 26', 'Anero Ribamontán al Monte', 664392904, NULL, '', '2020-10-01', '10', 2, '', '1'),
(72, 'Laura', 'Magdaleno', '', 'Bº Pelleja, 4', 'Heras Medio Cudeyo', 687921316, NULL, '', '2020-10-01', '12', 2, '', '1'),
(73, 'Víctor ', 'Crespo Vidal', '', 'Bº Rubayo, 44', 'Ceceñas Medio Cudeyo', 661187593, NULL, '', '2020-10-01', '12', 1, '', '1'),
(74, 'Lucía', 'Pérez Agudo', '', 'C/ Las Gándaras, 1', 'Arenal de Penagos', 640269926, NULL, '', '2020-10-01', '12', 1, '', '1'),
(75, 'Laura', 'Ortiz Alonso', '', 'Paseo de Velasco, 39, 3º G', 'Liérganes', 673353424, NULL, '', '2020-10-01', '12', 1, '', '1'),
(76, 'Berta ', 'Gómez Trueba', '', 'C/ Mies de San Martín, 8, 1º A', 'Liérganes', 649915434, NULL, '', '2020-10-01', '12', 1, '', '1'),
(77, 'Ruth', 'Crespo Iturralde', '', 'Bº El Pozuco, 5', 'Entrambasaguas', 680353368, NULL, '', '2020-10-01', '12', 1, '', '1'),
(78, 'Lucía', 'Blanco Sañudo', '', 'C/ El Crucero, 20', 'Solares (Medio Cudeyo)', 634474327, NULL, '', '2020-10-01', '12', 1, '', '1'),
(79, 'Paula ', 'Menéndez', '', 'C/ Trillagua, 173', 'Ceceñas Medio Cudeyo', 679702975, NULL, '', '2020-10-01', '12', 1, '', '1'),
(80, 'Sonia ', 'Colina Abascal', '', 'Bº La Rañada, 9B, 3A', 'Entrambasaguas', 659893268, NULL, '', '2020-10-01', '12', 1, '', '1'),
(81, 'Ariadna ', 'Gómez Maya', '', 'Bº Pedredo, 134', 'Hermosa Medio Cudeyo', 633329171, NULL, '', '2020-10-01', '12', 1, '', '1'),
(82, 'Mercy', 'Eghaghe', '', 'C/ Emilio Maza Cifrián', 'Solares (Medio Cudeyo)', 646803292, NULL, '', '2020-10-01', '5', 4, '', '1'),
(83, 'Claudia', 'López Miranda', '', 'C/ Los Morriones, 3', 'Sobarzo (Penagos)', 619743524, NULL, '', '2020-10-01', '4', 14, '', '1'),
(84, 'Sofía', 'Sánchez Martínez', '', 'Solares', 'Medio Cudeyo', 622456302, NULL, '', '2020-10-01', '12', 3, '', '1'),
(85, 'Hugo ', 'Maza', '', 'Solares ', 'Cantabria', 650847645, NULL, '', '1970-01-01', '5', 4, '', '1'),
(86, 'Sara ', 'Hoz Crespo', '', 'C/ Huyo, 18', 'Llanos de Penagos', 690127735, NULL, '', '2020-10-01', '8', 1, '', '1'),
(87, 'Víctor ', 'Pascua Sáiz', '', 'Bº La Estación, 181, 3º', 'Liaño de Villaescusa', 669449946, NULL, '', '2020-10-01', '12', 1, '', '1'),
(88, 'Laura', 'Álvarez Barquinero', '', 'Avda. Ramón Pelayo, 12', 'Valdecilla Medio Cudeyo', 629874135, NULL, '', '2020-10-01', '11', 2, '', '1'),
(89, 'Alexandra ', 'García Vargas', '', 'C/ Los Tilos, 1º B, 3º C ', 'Solares Medio Cudeyo', 665415449, 722697038, '', '2020-10-01', '11', 1, '', '1'),
(90, 'Robert', 'Pérez Alcántara', '', 'C/ Francisco Perojo, 135 ', 'Solares Medio Cudeyo', 666819411, NULL, '', '2020-10-01', '9', 1, '', '1'),
(91, 'Manuel', 'Rivas García', '', 'C/ El Crucero, 16', 'Solares Medio Cudeyo', NULL, 942521216, '', '2020-10-01', '9', 1, '', '1'),
(92, 'Iria ', 'Ruiz Torrecilla', '', 'Paseo de Velasco, 35', 'Liérganes', 636696751, NULL, '', '2020-10-01', '10', 3, '', '1'),
(93, 'Rogelio', 'Pérez', '', 'Bº Padierne, 50', 'Navajeda Entrambasaguas ', NULL, NULL, '', '2020-10-01', '9', 1, '', '1'),
(94, 'David ', 'Crespo Iturralde', '', 'Entrambasaguas', '', 680353368, NULL, '', '2020-10-01', '9', 1, '', ''),
(95, 'Érika', 'Lavín Azcune ', '', 'Merilla', 'San Roque de Riomiera', 637144972, NULL, '', '2020-10-01', '12', 1, '', '1'),
(96, 'Lucía', 'Calleja Ruiz', '', 'C/ Ángel de la Hoz, 17', 'Solares Medio Cudeyo', 608520574, NULL, '', '2020-10-01', '12', 1, '', '1'),
(97, 'José Ramón ', 'Madrazo Arco', '', 'C/ Emilio Maza Cifrián', 'Solares Medio Cudeyo', 657679775, NULL, '', '2020-10-01', '9', 1, '', '1'),
(98, 'David ', 'Lavín', '', 'Bº La Rañada, 13, 7 C', 'Entrambasaguas', 609374773, NULL, '', '2020-10-01', '11', 1, '', '1'),
(99, 'Laura Valentina', 'Ramos Garrido', '', 'Río Laya, 2, 3º izq. ', 'Solares Medio Cudeyo ', 641854592, NULL, '', '2020-10-01', '11', 1, '', '1'),
(100, 'Desirée ', 'Gómez Dertiano', '', 'Avda. Oviedo, 4 1º A', 'Solares Medio Cudeyo', NULL, NULL, '', '2020-10-01', '9', 1, '', '1'),
(101, 'Marcos', 'Sastre Olazarán', '', 'Bº Monasterio, 36', 'Pámanes Liérganes', 611144949, NULL, '', '2020-10-01', '9', 1, '', '1'),
(102, 'Mario ', 'Gándara Laza ', '', 'C/ Cubiles, 19', 'Setién Marina de Cudeyo', 640122670, NULL, '', '2020-10-01', '11', 2, '', '1'),
(103, 'Verónica ', 'Cobo Madrazo', '', 'Bº Liegra, 2 ', 'Beranga Hazas de Cesto', 617312081, NULL, '', '2020-10-01', '9', 8, '', '1'),
(104, 'María José ', 'Ocampo Pajoy', '', 'Bº Río Laya, 2, nº 17', 'Solares Medio Cudeyo ', 637492544, NULL, '', '2020-10-01', '9', 3, '', '1'),
(105, 'Luis', 'Cruz Carrera', '', 'Bº Voz al Rey, 26', 'Heras Medio Cudeyo', 646581400, NULL, '', '2020-10-01', '10', 2, '', '1'),
(106, 'Pedro ', 'Abad Delgado', '', 'Bº El Escoral, 26 El Bosque ', 'Entrambasaguas', 644586147, NULL, '', '2020-10-01', '11', 3, '', '1'),
(107, 'Emiliano ', 'Del Arco', '', 'Avda. Oviedo', 'Solares Medio Cudeyo', 600699125, NULL, '', '2020-10-01', '10', 1, '', '1'),
(108, 'Gabriel ', 'Souto Trigo', '', 'Urb. La Ermita, 9', 'Ceceñas Medio Cudeyo', 675210369, NULL, '', '2020-10-01', '11', 1, '', '1'),
(109, 'Nuria ', 'Cobo Cobo', '', 'Argomilla, 84', 'Rubayo Marina de Cudeyo', 622420910, NULL, '', '2020-10-01', '11', 2, '', '1'),
(110, 'Alba ', 'Cadelo Ruiz', '', 'Bº Las Cagigas, 16', 'Heras Medio Cudeyo', 692873607, NULL, '', '2020-10-01', '11', 2, '', '1'),
(111, 'Marcos', 'Barros Cuesta', '', 'Bº La Sota, 19, 2º A', 'Anero Ribamontán al Monte', 605316558, NULL, '', '2020-10-01', '13', 18, '', '1'),
(112, 'Helena ', 'Alonso Arnáiz', '', 'C/ Constitución, 3, 5º izq.', 'Solares Medio Cudeyo', 692024808, NULL, '', '2020-10-01', '', 10, '', '1'),
(113, 'Roraima', 'Noguera Rivero ', '', 'Avda. Oviedo, 20 B', 'Solares Medio Cudeyo', 650558969, NULL, '', '2020-10-01', '', 15, '', '1'),
(114, 'Natalia', 'Gándara Urrusuno', '', 'Bº Vía, 11', 'Pontones Ribamontán al Monte', 682371285, NULL, '', '2020-10-01', '9', 2, '', '1'),
(115, 'Andrea', 'Quintanilla Calleja', '', 'Carretera Irún-La Coruña, 19', 'La Penilla Santa María de Cayón', 659762458, NULL, '', '2020-10-01', '8', 11, '', '1'),
(116, 'Catalina', 'Gándara Urrusuno', '', 'Bº Vía, 11', 'Pontones Ribamontán al Monte', 619053010, NULL, '', '2020-10-01', '13', 10, '', '1'),
(117, 'Juan José ', 'Lavín Martínez', '', 'C/ Puente romano, 8', 'Liérganes', 620716371, NULL, '', '2020-10-01', '8', 3, '', '1'),
(118, 'Manuel', 'Tricio Cagigas', '', 'Bº San Roque, 117 B', 'Elechas Marina de Cudeyo', 648968511, NULL, '', '2020-10-01', '12', 2, '', '1'),
(119, 'Diego', 'Cruz Fernández', '', 'La Tejera, 4', 'Entrambasaguas', 676276659, NULL, '', '2020-10-01', '7', 1, '', '1'),
(120, 'Cristian', 'Santamaría González', '', 'La Rañada 14, 5º A, 3º C', 'Entrambasaguas', 654917975, NULL, '', '2020-10-01', '7', 7, '', '1'),
(121, 'Mariela', 'Fernández Fernández', '', 'Solares', 'Medio Cudeyo', 648845181, NULL, '', '2020-10-01', '7', 2, '', '1'),
(122, 'Fiorella Jazmín ', 'Gayoso Candia', '', 'Los Tilos, 1º B, 2º A ', 'Solares Medio Cudeyo', 611639981, NULL, '', '2020-10-01', '7', 1, '', '1'),
(123, 'Marina', 'Viñe Fernández', '', 'C/ Crucero, 11, nº 4', 'Solares Medio Cudeyo', 615361248, NULL, '', '2020-10-01', '8', 3, '', '1'),
(124, 'Marcos', 'Fernández Esquinas', '', 'Solares', 'Medio Cudeyo', 699270269, NULL, '', '2020-10-01', '6', 16, '', '1'),
(125, 'Adrián ', 'Sierra', '', 'Solares ', 'Medio Cudeyo', 669630374, NULL, '', '2020-10-01', '5', 4, '', '1'),
(126, 'Sandra', 'Yanguas Revuelta', '', 'Paseo del ayuntamiento, 93', 'Valdecilla Medio Cudeyo', 685441061, NULL, '', '2020-10-01', '9', 1, '', '1'),
(127, 'Paula ', 'Herrera Caballero', '', 'Bº Rioz, 158', 'Sobremazas', 660716639, NULL, '', '2020-10-01', '9', 1, '', '1'),
(128, 'Rocío', 'Bravo Cacicedo', '', 'Bº La Gándara, 102', 'Heras Medio Cudeyo', 663494183, NULL, '', '2020-10-01', '11', 2, '', '1'),
(129, 'Miguel Ángel ', 'Torres Ureña', '', 'C/ San Pedruco, 8', 'Solares Medio Cudeyo', 601258174, NULL, '', '2020-10-01', '11', 1, '', '1'),
(130, 'Marcos', 'Galavis Martínez', '', 'C/ Pedregajas, 3 ', 'Solares Medio Cudeyo', 630191161, NULL, '', '2020-10-01', '10', 1, '', '1'),
(131, 'Aurora', 'Viscarolasaga Canales', '', 'Bº La Rañada 10 F, 3º B ', 'Entrambasaguas', 679130309, NULL, '', '2020-10-01', '5', 4, '', '1'),
(132, 'Yaiza', 'Salvarrey Flores', '', 'Avda. Calvo Sotelo, 14 1º D ', 'Solares Medio Cudeyo', 654157979, NULL, '', '2020-10-01', '5', 4, '', '1'),
(133, 'Jimena ', 'Morao de la Hoz', '', 'Hermosa', 'Medio Cudeyo', 621036844, NULL, '', '2020-10-01', '4', 4, '', '1'),
(134, 'Juan ', 'Ruiz Freire', '', 'Bº La Riva, 173', 'Villanueva de Villaescusa ', 654219833, NULL, '', '2020-10-01', '8', 3, '', '1'),
(135, 'Noelia ', 'Blanco Turanzas', '', 'Bº Trapa, 32', 'Villaverde de Pontones Rib al Monte', 652841247, 722531920, '', '2020-10-01', '12', 2, '', '1'),
(136, 'Alisson', 'Manrique Pajoy', '', 'C/ Río Laya', 'Solares Medio Cudeyo', 677296473, NULL, '', '2020-10-01', '6', 3, '', '1'),
(137, 'Jana ', 'Arenal Ruiz', '', 'Solares', 'Medio Cudeyo', 600677685, NULL, '', '2020-10-01', '9', 3, '', '1'),
(138, 'Lara ', 'Castillo García', '', 'C/ Mercadillo, 40', 'Liérganes', NULL, NULL, '', '2020-10-01', '6', 5, '', '1'),
(139, 'Diego', 'Pacheco Rodríguez', '', 'C/ Fernández Rañada, 10 ', 'Solares Medio Cudeyo', 630705090, NULL, '', '2020-10-01', '9', 3, '', '1'),
(140, 'Laura', 'Cruz Taboada', '', 'Sobremazas', 'Medio Cudeyo', NULL, NULL, '', '2020-10-01', '9', 3, '', '1'),
(141, 'Ruth', 'Hernández Crespo', '', 'C/ La Teja, 87 ', 'Orejo Marina de Cudeyo', 667743229, NULL, '', '2020-10-01', '6', 7, '', '1'),
(142, 'Lucía', 'Perojo', '', 'Ceceñas', 'Medio Cudeyo', 656947565, NULL, '', '2020-10-01', '12', 17, '', '1'),
(143, 'Ángela', 'Barquinero', '', 'Ceceñas', 'Medio Cudeyo', 699080201, NULL, '', '2020-10-01', '11', 2, '', '1'),
(144, 'Rebeca', 'Hidalgo Navarro', '', 'C/ El Soto, 24', 'Galizano Ribamontán al Mar', 686899599, NULL, '', '2020-10-01', '11', 2, '', '1'),
(145, 'Candela ', 'Pérez Rebanal', '', 'Bº La Sierra, 18 A', 'Entrambasaguas', 698932052, NULL, '', '2020-10-01', '11', 1, '', '1'),
(146, 'Leonor', 'Vera San Emeterio', '', 'Avda. Trasmiera, 6, 4º B', 'Heras Medio Cudeyo', 621357363, NULL, '', '2020-10-01', '11', 2, '', '1'),
(147, 'Alba', 'Cobo Riva', '', 'C/ Muelle del rey, 36', 'Somo Ribamontán al Mar', 626200584, NULL, '', '2020-10-01', '11', 2, '', '1'),
(148, 'Karlota', 'Bellut Herrera', '', 'Bº La Ensera, 5 Ánaz', 'Medio Cudeyo', 673476028, NULL, '', '2020-11-01', '9', 1, '', '1'),
(149, 'lucía', 'Ortiz', '', 'Avda. Oviedo, 20 G 1º A ', 'Solares Medio Cudeyo', 659621814, NULL, '', '2020-11-01', '6', 4, '', '1'),
(150, 'Óscar ', 'Pérez Bárcena', '', 'C/ Ángel de la Hoz, 23', 'Solares Medio Cudeyo', 608260094, NULL, '', '2020-11-01', '12', 1, '', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignaturas`
--

CREATE TABLE `asignaturas` (
  `id` int(11) NOT NULL,
  `asignatura` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `asignaturas`
--

INSERT INTO `asignaturas` (`id`, `asignatura`) VALUES
(1, 'DIBUJO'),
(2, 'ECONOMIA'),
(3, 'FISICA QUIMICA & MATEMÁTICAS'),
(6, 'INGLES'),
(8, 'VARIAS ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centros`
--

CREATE TABLE `centros` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `localidad` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `centros`
--

INSERT INTO `centros` (`id`, `nombre`, `localidad`) VALUES
(1, 'IES RICARDO BERNARDO', 'SOLARES'),
(2, 'IES LA GRANJA', 'HERAS'),
(3, 'TORREANAZ', 'ANAZ'),
(4, 'CP MARQUES DE VALDECILLA', 'VALDECILLA'),
(5, 'CP EUGENIO PEROJO', 'LIERGANES'),
(6, 'CP AGUANAZ', 'ENTRAMBASAGUAS'),
(7, 'APOSTOLADO DEL SAGRADO CORAZON', 'CECEÑAS'),
(8, 'IES SAN MIGUEL DE MERUELO', 'MERUELO'),
(9, 'UNIVERSIDAD EUROPEA DEL ATLANTICO', 'SANTANDER'),
(10, 'UNIVERSIDAD DE CANTABRIA', 'SANTANDER'),
(11, 'IES LOPE DE VEGA', 'SANTA MARIA DE CAYON'),
(12, 'CEIP TRASMIERA', 'HOZ DE ANERO'),
(13, 'IES ASTILLERO', 'El Astillero'),
(14, 'CEIP GERARDO DIEGO', 'Santa María de Cayón'),
(15, 'IES PEÑACASTILLO', 'SANTANDER'),
(16, 'CEIP NUESTRA SEÑORA DE LATAS', 'SOMO'),
(17, 'SANTA CLARA', 'Santander'),
(18, 'ESCUELA OFICIAL DE IDIOMAS (EOI)', 'Santander');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clases`
--

CREATE TABLE `clases` (
  `id` int(11) NOT NULL,
  `curso` int(11) DEFAULT NULL,
  `asignatura` int(11) DEFAULT NULL,
  `importe` float DEFAULT NULL,
  `observaciones` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clases`
--

INSERT INTO `clases` (`id`, `curso`, `asignatura`, `importe`, `observaciones`) VALUES
(1, 13, 6, 45, ''),
(2, 14, 3, 45, ''),
(3, 16, 8, 45, ''),
(4, 15, 8, 45, ''),
(5, 17, 2, 45, ''),
(6, 18, 8, 45, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_bancarias`
--

CREATE TABLE `cuentas_bancarias` (
  `id` int(11) NOT NULL,
  `alumno` int(11) DEFAULT NULL,
  `iban` varchar(4) DEFAULT NULL,
  `entidad` int(4) DEFAULT NULL,
  `oficina` int(4) DEFAULT NULL,
  `dc` int(2) DEFAULT NULL,
  `cuenta` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id` int(11) NOT NULL,
  `curso` varchar(50) DEFAULT NULL,
  `descripcion` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `curso`, `descripcion`) VALUES
(1, '1P', '1º PRIMARIA'),
(2, '2P', '2º PRIMARIA'),
(3, '3P', '3º PRIMARIA'),
(4, '4P', '4º PRIMARIA'),
(5, '5P', '5º PRIMARIA'),
(6, '6P', '6º PRIMARIA'),
(7, '1ESO', '1º ESO'),
(8, '2ESO', '2º ESO'),
(9, '3ESO', '3º ESO'),
(10, '4ESO', '4º ESO'),
(11, '1BAC', '1º BACHILLERATO'),
(12, '2BAC', '2º BACHILLERATO'),
(13, 'Sara Inglés ', ''),
(14, 'Marisol', ''),
(15, 'Noelia ', ''),
(16, 'Silvia Ruiz ', ''),
(17, 'Sara Vega Economía', ''),
(18, 'Zulema', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imparten`
--

CREATE TABLE `imparten` (
  `id` int(11) NOT NULL,
  `profesor` int(11) DEFAULT NULL,
  `asignatura` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material`
--

CREATE TABLE `material` (
  `id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `alumno` int(11) DEFAULT NULL,
  `concepto` varchar(100) DEFAULT NULL,
  `importe` float(4,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `material`
--

INSERT INTO `material` (`id`, `fecha`, `alumno`, `concepto`, `importe`) VALUES
(2, '2020-11-01', 38, 'fotocopias octubre 2020', 1.00),
(3, '2020-11-01', 39, 'fotocopias octubre 2020', 1.00),
(5, '2020-11-01', 116, 'fotocopias octubre 2020', 1.50),
(6, '2020-11-01', 112, 'fotocopias octubre 2020', 1.00),
(7, '2020-11-01', 113, 'fotocopias octubre 2020', 4.00),
(8, '2020-11-01', 28, 'fotocopias octubre 2020', 2.00),
(9, '2020-11-01', 102, 'fotocopias octubre 2020', 0.50),
(10, '2020-11-01', 15, 'fotocopias octubre 2020', 3.00),
(11, '2020-11-01', 14, 'fotocopias octubre 2020', 1.00),
(12, '2020-11-01', 60, 'fotocopias octubre 2020', 1.50),
(13, '2020-11-01', 83, 'fotocopias octubre 2020', 1.00),
(14, '2020-11-01', 139, 'fotocopias octubre 2020', 1.00),
(15, '2020-11-01', 103, 'fotocopias octubre 2020', 1.00),
(16, '2020-11-01', 140, 'fotocopias octubre 2020', 1.00),
(17, '2020-11-01', 117, 'fotocopias octubre 2020', 2.00),
(18, '2020-11-01', 50, 'fotocopias octubre 2020', 2.00),
(19, '2020-11-01', 51, 'fotocopias octubre 2020', 2.00),
(20, '2020-11-01', 53, 'fotocopias octubre 2020', 1.00),
(21, '2020-11-01', 61, 'fotocopias octubre 2020', 2.50),
(22, '2020-11-01', 17, 'fotocopias octubre 2020', 2.00),
(23, '2020-11-01', 115, 'fotocopias octubre 2020', 1.00),
(24, '2020-11-01', 136, 'fotocopias octubre 2020', 1.00),
(25, '2020-11-01', 138, 'fotocopias octubre 2020', 1.00),
(26, '2020-11-01', 33, 'fotocopias octubre 2020', 2.00),
(27, '2020-11-01', 42, 'fotocopias octubre 2020', 0.50),
(28, '2020-11-01', 11, 'fotocopias octubre 2020', 1.00),
(29, '2020-11-01', 10, 'fotocopias octubre 2020', 1.00),
(30, '2020-11-01', 12, 'fotocopias octubre 2020', 1.00),
(31, '2020-11-01', 130, 'fotocopias octubre 2020', 1.00),
(32, '2020-11-01', 107, 'fotocopias octubre 2020', 1.00),
(33, '2020-11-01', 49, 'fotocopias octubre 2020', 1.00),
(34, '2020-11-01', 56, 'fotocopias octubre 2020', 2.00),
(35, '2020-11-01', 24, 'fotocopias octubre 2020', 1.00),
(36, '2020-11-01', 18, 'fotocopias octubre 2020', 1.00),
(37, '2020-11-01', 47, 'fotocopias octubre 2020', 1.00),
(38, '2020-11-01', 108, 'fotocopias octubre 2020', 1.00),
(39, '2020-11-01', 21, 'fotocopias octubre 2020', 1.00),
(40, '2020-11-01', 20, 'fotocopias octubre 2020', 1.00),
(41, '2020-11-01', 26, 'fotocopias octubre 2020', 1.00),
(42, '2020-11-01', 27, 'fotocopias octubre 2020', 1.50),
(43, '2020-11-01', 110, 'fotocopias octubre 2020', 0.50),
(44, '2020-11-01', 109, 'fotocopias octubre 2020', 0.50),
(45, '2020-11-01', 29, 'fotocopias octubre 2020', 1.50),
(46, '2020-11-01', 30, 'fotocopias octubre 2020', 1.50),
(47, '2020-11-01', 31, 'fotocopias octubre 2020', 1.50),
(48, '2020-11-01', 32, 'fotocopias octubre 2020', 0.50),
(49, '2020-11-01', 34, 'fotocopias octubre 2020', 1.50),
(50, '2020-11-01', 35, 'fotocopias octubre 2020', 1.50),
(51, '2020-11-01', 48, 'fotocopias octubre 2020', 1.50),
(52, '2020-11-01', 150, 'fotocopias octubre 2020', 1.50),
(53, '2020-11-01', 135, 'fotocopias octubre 2020', 0.50),
(54, '2020-11-01', 86, 'fotocopias octubre 2020', 1.00),
(55, '2020-11-01', 123, 'fotocopias octubre 2020', 1.00),
(56, '2020-11-01', 54, 'fotocopias octubre 2020', 1.00),
(57, '2020-11-01', 121, 'fotocopias octubre 2020', 1.00),
(58, '2020-11-01', 125, 'fotocopias octubre 2020', 1.00),
(59, '2020-11-01', 124, 'fotocopias octubre 2020', 1.00),
(60, '2020-11-01', 45, 'fotocopias octubre 2020', 0.50),
(61, '2020-11-01', 87, 'fotocopias octubre 2020', 0.50),
(62, '2020-11-01', 62, 'fotocopias octubre 2020', 1.00),
(63, '2020-11-01', 84, 'fotocopias octubre 2020', 1.00),
(64, '2020-11-01', 80, 'fotocopias octubre 2020', 1.00),
(65, '2020-11-01', 79, 'fotocopias octubre 2020', 1.00),
(66, '2020-11-01', 73, 'fotocopias octubre 2020', 1.00),
(67, '2020-11-01', 78, 'fotocopias octubre 2020', 2.00),
(68, '2020-11-01', 96, 'fotocopias octubre 2020', 1.00),
(69, '2020-11-01', 94, 'fotocopias octubre 2020', 2.00),
(70, '2020-11-01', 81, 'fotocopias octubre 2020', 1.00),
(71, '2020-11-01', 74, 'fotocopias octubre 2020', 1.50),
(72, '2020-11-01', 75, 'fotocopias octubre 2020', 1.50),
(73, '2020-11-01', 76, 'fotocopias octubre 2020', 1.50),
(74, '2020-11-01', 65, 'fotocopias octubre 2020', 0.50),
(75, '2020-11-01', 66, 'fotocopias octubre 2020', 0.50),
(76, '2020-11-01', 89, 'fotocopias octubre 2020', 1.50),
(77, '2020-11-01', 99, 'fotocopias octubre 2020', 1.50),
(78, '2020-11-01', 98, 'fotocopias octubre 2020', 1.50),
(79, '2020-11-01', 129, 'fotocopias octubre 2020', 2.50),
(80, '2020-11-01', 64, 'fotocopias octubre 2020', 1.50),
(81, '2020-11-01', 145, 'fotocopias octubre 2020', 0.50),
(82, '2020-11-01', 59, 'fotocopias octubre 2020', 0.50),
(83, '2020-11-01', 68, 'fotocopias octubre 2020', 1.00),
(84, '2020-11-01', 63, 'fotocopias octubre 2020', 1.00),
(85, '2020-11-01', 43, 'fotocopias octubre 2020', 0.50);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriculas`
--

CREATE TABLE `matriculas` (
  `id` int(11) NOT NULL,
  `alumno` int(11) DEFAULT NULL,
  `clase` int(11) DEFAULT NULL,
  `alta` date DEFAULT NULL,
  `baja` date DEFAULT NULL,
  `observaciones` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `matriculas`
--

INSERT INTO `matriculas` (`id`, `alumno`, `clase`, `alta`, `baja`, `observaciones`) VALUES
(1, 1, 2, '2020-10-01', NULL, ''),
(2, 5, 2, '2020-10-01', NULL, ''),
(3, 7, 2, '2020-10-01', NULL, ''),
(4, 8, 2, '2020-10-01', NULL, ''),
(5, 9, 2, '2020-10-01', NULL, ''),
(6, 11, 2, '2020-10-01', NULL, ''),
(7, 10, 2, '2020-10-01', NULL, ''),
(8, 12, 2, '2020-10-01', NULL, ''),
(9, 13, 2, '2020-10-01', NULL, ''),
(10, 14, 2, '2020-10-01', NULL, ''),
(11, 15, 2, '2020-10-01', NULL, ''),
(12, 16, 2, '2020-10-01', NULL, ''),
(13, 17, 2, '2020-10-01', NULL, ''),
(14, 18, 2, '2020-10-01', NULL, ''),
(15, 20, 2, '2020-10-01', NULL, ''),
(16, 21, 2, '2020-10-01', NULL, ''),
(17, 22, 2, '2020-10-01', NULL, ''),
(18, 23, 2, '2020-10-01', NULL, ''),
(19, 24, 2, '2020-10-01', NULL, ''),
(20, 25, 2, '2020-10-01', NULL, ''),
(21, 27, 2, '2020-10-01', NULL, ''),
(22, 26, 2, '2020-10-01', NULL, ''),
(23, 28, 2, '2020-10-01', NULL, ''),
(24, 29, 2, '2020-10-01', NULL, ''),
(25, 30, 2, '2020-10-01', NULL, ''),
(26, 31, 2, '2020-10-01', NULL, ''),
(27, 32, 2, '2020-10-01', NULL, ''),
(28, 33, 2, '2020-10-01', NULL, ''),
(29, 34, 2, '2020-10-01', NULL, ''),
(30, 35, 2, '2020-10-01', NULL, ''),
(31, 37, 4, '2020-10-01', NULL, ''),
(32, 38, 1, '2020-10-01', NULL, ''),
(33, 39, 1, '2020-10-01', NULL, ''),
(34, 41, 4, '2020-10-01', NULL, ''),
(35, 42, 4, '2020-10-01', NULL, ''),
(36, 43, 4, '2020-10-01', NULL, ''),
(37, 44, 4, '2020-10-01', NULL, ''),
(38, 45, 4, '2020-10-01', NULL, ''),
(39, 46, 4, '2020-10-01', NULL, ''),
(40, 47, 4, '2020-10-01', NULL, ''),
(41, 48, 4, '2020-10-01', NULL, ''),
(42, 49, 4, '2020-10-01', NULL, ''),
(43, 50, 4, '2020-10-01', NULL, ''),
(44, 51, 4, '2020-10-01', NULL, ''),
(45, 52, 4, '2020-10-01', NULL, ''),
(46, 53, 4, '2020-10-01', NULL, ''),
(47, 54, 4, '2020-10-01', NULL, ''),
(48, 55, 4, '2020-10-01', NULL, ''),
(49, 56, 4, '2020-10-01', NULL, ''),
(50, 57, 4, '2020-10-01', '2020-10-31', ''),
(51, 58, 4, '2020-10-01', NULL, ''),
(52, 59, 4, '2020-10-01', NULL, ''),
(53, 60, 4, '2020-10-01', NULL, ''),
(54, 61, 4, '2020-10-01', NULL, ''),
(55, 62, 4, '2020-10-01', NULL, ''),
(56, 63, 4, '2020-10-01', NULL, ''),
(57, 64, 4, '2020-10-01', NULL, ''),
(58, 65, 4, '2020-10-01', NULL, ''),
(59, 66, 4, '2020-10-01', NULL, ''),
(60, 67, 4, '2020-10-01', NULL, ''),
(61, 68, 4, '2020-10-01', NULL, ''),
(62, 69, 4, '2020-10-01', NULL, ''),
(63, 70, 4, '2020-10-01', NULL, ''),
(64, 71, 4, '2020-10-01', NULL, ''),
(65, 72, 4, '2020-10-01', NULL, ''),
(66, 73, 4, '2020-10-01', NULL, ''),
(67, 74, 4, '2020-10-01', NULL, ''),
(68, 75, 4, '2020-10-01', NULL, ''),
(69, 76, 4, '2020-10-01', NULL, ''),
(70, 77, 4, '2020-10-01', NULL, ''),
(71, 78, 4, '2020-10-01', NULL, ''),
(72, 79, 4, '2020-10-01', NULL, ''),
(73, 80, 4, '2020-10-01', NULL, ''),
(74, 81, 4, '2020-10-01', NULL, ''),
(75, 82, 4, '2020-10-01', NULL, ''),
(76, 83, 4, '2020-10-01', NULL, ''),
(77, 84, 4, '2020-10-01', NULL, ''),
(78, 85, 4, '2020-10-01', NULL, ''),
(79, 86, 4, '2020-10-01', NULL, ''),
(80, 87, 4, '2020-10-01', NULL, ''),
(81, 88, 4, '2020-10-01', NULL, ''),
(82, 89, 4, '2020-10-01', NULL, ''),
(83, 90, 4, '2020-10-01', NULL, ''),
(84, 91, 4, '2020-10-01', NULL, ''),
(85, 92, 4, '2020-10-01', NULL, ''),
(86, 93, 4, '2020-10-01', NULL, ''),
(87, 94, 4, '2020-10-01', NULL, ''),
(88, 95, 4, '2020-10-01', NULL, ''),
(89, 96, 4, '2020-10-01', NULL, ''),
(90, 97, 4, '2020-10-01', NULL, ''),
(91, 98, 4, '2020-10-01', NULL, ''),
(92, 99, 4, '2020-10-01', NULL, ''),
(93, 100, 4, '2020-10-01', NULL, ''),
(94, 101, 4, '2020-10-01', NULL, ''),
(95, 102, 4, '2020-10-01', NULL, ''),
(96, 103, 4, '2020-10-01', NULL, ''),
(97, 104, 4, '2020-10-01', NULL, ''),
(98, 105, 2, '2020-10-01', NULL, ''),
(99, 106, 2, '2020-10-01', NULL, ''),
(100, 107, 2, '2020-10-01', NULL, ''),
(101, 108, 2, '2020-10-01', NULL, ''),
(102, 109, 2, '2020-10-01', NULL, ''),
(103, 110, 2, '2020-10-01', NULL, ''),
(104, 111, 1, '2020-10-01', NULL, ''),
(105, 112, 1, '2020-10-01', NULL, ''),
(106, 113, 1, '2020-10-01', NULL, ''),
(107, 114, 1, '2020-10-01', NULL, ''),
(108, 115, 1, '2020-10-01', NULL, ''),
(109, 116, 1, '2020-10-01', NULL, ''),
(110, 117, 1, '2020-10-01', NULL, ''),
(111, 118, 4, '2020-10-01', NULL, ''),
(112, 119, 4, '2020-10-01', NULL, ''),
(113, 120, 4, '2020-10-01', NULL, ''),
(114, 121, 4, '2020-10-01', NULL, ''),
(115, 122, 4, '2020-10-01', NULL, ''),
(116, 123, 4, '2020-10-01', NULL, ''),
(117, 124, 4, '2020-10-01', NULL, ''),
(118, 125, 4, '2020-10-01', NULL, ''),
(119, 126, 4, '2020-10-01', NULL, ''),
(120, 127, 4, '2020-10-01', NULL, ''),
(121, 128, 4, '2020-10-01', NULL, ''),
(122, 129, 4, '2020-10-01', NULL, ''),
(123, 130, 2, '2020-10-01', NULL, ''),
(124, 131, 4, '2020-10-01', NULL, ''),
(125, 132, 4, '2020-10-01', NULL, ''),
(126, 133, 4, '2020-10-01', NULL, ''),
(127, 134, 4, '2020-10-01', NULL, ''),
(128, 135, 4, '2020-10-01', NULL, ''),
(129, 136, 4, '2020-10-01', NULL, ''),
(130, 137, 4, '2020-10-01', NULL, ''),
(131, 138, 4, '2020-10-01', NULL, ''),
(132, 139, 4, '2020-10-01', NULL, ''),
(133, 140, 4, '2020-10-01', NULL, ''),
(134, 141, 4, '2020-10-01', NULL, ''),
(135, 142, 4, '2020-10-01', NULL, ''),
(136, 143, 4, '2020-10-01', NULL, ''),
(137, 144, 4, '2020-10-01', NULL, ''),
(138, 145, 4, '2020-10-01', NULL, ''),
(139, 146, 4, '2020-10-01', NULL, ''),
(140, 147, 4, '2020-10-01', NULL, ''),
(141, 148, 4, '2020-11-01', NULL, ''),
(142, 149, 3, '2020-11-01', NULL, ''),
(143, 150, 4, '2020-11-01', NULL, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesores`
--

CREATE TABLE `profesores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(200) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `poblacion` varchar(100) DEFAULT NULL,
  `dni` varchar(9) DEFAULT NULL,
  `movil` int(11) DEFAULT NULL,
  `fijo` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `alta` date DEFAULT NULL,
  `baja` date DEFAULT NULL,
  `observaciones` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recibos`
--

CREATE TABLE `recibos` (
  `id` int(11) NOT NULL,
  `matricula` int(11) DEFAULT NULL,
  `emision` date DEFAULT NULL,
  `mes` int(2) DEFAULT NULL,
  `anyo` int(4) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `reducido` int(3) DEFAULT NULL,
  `importe` float(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `recibos`
--

INSERT INTO `recibos` (`id`, `matricula`, `emision`, `mes`, `anyo`, `estado`, `reducido`, `importe`) VALUES
(1, 1, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(2, 2, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(3, 3, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(4, 4, '2020-10-01', 10, 2020, 1, NULL, 55.00),
(5, 5, '2020-10-01', 10, 2020, 1, NULL, 0.00),
(6, 6, '2020-10-01', 10, 2020, 1, NULL, 90.00),
(7, 7, '2020-10-01', 10, 2020, 0, NULL, 55.00),
(8, 8, '2020-10-01', 10, 2020, 1, NULL, 55.00),
(9, 9, '2020-10-01', 10, 2020, 1, NULL, 55.00),
(10, 10, '2020-10-01', 10, 2020, 1, NULL, 90.00),
(11, 11, '2020-10-01', 10, 2020, 1, NULL, 160.00),
(12, 12, '2020-10-01', 10, 2020, 1, NULL, 0.00),
(13, 13, '2020-10-01', 10, 2020, 1, NULL, 100.00),
(14, 14, '2020-10-01', 10, 2020, 0, NULL, 60.00),
(15, 15, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(16, 16, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(17, 17, '2020-10-01', 10, 2020, 1, NULL, 0.00),
(18, 18, '2020-10-01', 10, 2020, 1, NULL, 0.00),
(19, 19, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(20, 20, '2020-10-01', 10, 2020, 1, NULL, 90.00),
(21, 21, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(22, 22, '2020-10-01', 10, 2020, 1, NULL, 85.00),
(23, 23, '2020-10-01', 10, 2020, 0, NULL, 185.00),
(24, 24, '2020-10-01', 10, 2020, 1, NULL, 105.00),
(25, 25, '2020-10-01', 10, 2020, 1, NULL, 105.00),
(26, 26, '2020-10-01', 10, 2020, 1, NULL, 105.00),
(27, 27, '2020-10-01', 10, 2020, 1, NULL, 80.00),
(28, 28, '2020-10-01', 10, 2020, 0, NULL, 130.00),
(29, 29, '2020-10-01', 10, 2020, 1, NULL, 105.00),
(30, 30, '2020-10-01', 10, 2020, 1, NULL, 105.00),
(31, 31, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(32, 32, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(33, 33, '2020-10-01', 10, 2020, 1, NULL, 40.00),
(34, 34, '2020-10-01', 10, 2020, 1, NULL, 0.00),
(35, 35, '2020-10-01', 10, 2020, 1, NULL, 35.00),
(36, 36, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(37, 37, '2020-10-01', 10, 2020, 1, NULL, 40.00),
(38, 38, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(39, 39, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(40, 40, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(41, 41, '2020-10-01', 10, 2020, 1, NULL, 105.00),
(42, 42, '2020-10-01', 10, 2020, 1, NULL, 90.00),
(43, 43, '2020-10-01', 10, 2020, 1, NULL, 90.00),
(44, 44, '2020-10-01', 10, 2020, 1, NULL, 90.00),
(45, 45, '2020-10-01', 10, 2020, 1, NULL, 0.00),
(46, 46, '2020-10-01', 10, 2020, 1, NULL, 80.00),
(47, 47, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(48, 48, '2020-10-01', 10, 2020, 1, NULL, 0.00),
(49, 49, '2020-10-01', 10, 2020, 1, NULL, 100.00),
(50, 50, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(51, 51, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(52, 52, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(53, 53, '2020-10-01', 10, 2020, 0, NULL, 90.00),
(54, 54, '2020-10-01', 10, 2020, 1, NULL, 130.00),
(55, 55, '2020-10-01', 10, 2020, 1, NULL, 85.00),
(56, 56, '2020-10-01', 10, 2020, 0, NULL, 45.00),
(57, 57, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(58, 58, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(59, 59, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(60, 60, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(61, 61, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(62, 62, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(63, 63, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(64, 64, '2020-10-01', 10, 2020, 0, NULL, 0.00),
(65, 65, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(66, 66, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(67, 67, '2020-10-01', 10, 2020, 1, NULL, 95.00),
(68, 68, '2020-10-01', 10, 2020, 1, NULL, 95.00),
(69, 69, '2020-10-01', 10, 2020, 1, NULL, 95.00),
(70, 70, '2020-10-01', 10, 2020, 1, NULL, 0.00),
(71, 71, '2020-10-01', 10, 2020, 1, NULL, 100.00),
(72, 72, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(73, 73, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(74, 74, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(75, 75, '2020-10-01', 10, 2020, 1, NULL, 0.00),
(76, 76, '2020-10-01', 10, 2020, 1, NULL, 160.00),
(77, 77, '2020-10-01', 10, 2020, 1, NULL, 85.00),
(78, 78, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(79, 79, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(80, 80, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(81, 81, '2020-10-01', 10, 2020, 1, NULL, 0.00),
(82, 82, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(83, 83, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(84, 84, '2020-10-01', 10, 2020, 0, NULL, 40.00),
(85, 85, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(86, 86, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(87, 87, '2020-10-01', 10, 2020, 1, NULL, 130.00),
(88, 88, '2020-10-01', 10, 2020, 0, NULL, 60.00),
(89, 89, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(90, 90, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(91, 91, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(92, 92, '2020-10-01', 10, 2020, 0, NULL, 95.00),
(93, 93, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(94, 94, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(95, 95, '2020-10-01', 10, 2020, 1, NULL, 80.00),
(96, 96, '2020-10-01', 10, 2020, 1, NULL, 80.00),
(97, 97, '2020-10-01', 10, 2020, 1, NULL, 70.00),
(98, 98, '2020-10-01', 10, 2020, 0, NULL, 55.00),
(99, 99, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(100, 100, '2020-10-01', 10, 2020, 1, NULL, 55.00),
(101, 101, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(102, 102, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(103, 103, '2020-10-01', 10, 2020, 1, NULL, 60.00),
(104, 104, '2020-10-01', 10, 2020, 0, NULL, 20.00),
(105, 105, '2020-10-01', 10, 2020, 0, NULL, 45.00),
(106, 106, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(107, 107, '2020-10-01', 10, 2020, 1, NULL, 80.00),
(108, 108, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(109, 109, '2020-10-01', 10, 2020, 1, NULL, 80.00),
(110, 110, '2020-10-01', 10, 2020, 0, NULL, 90.00),
(111, 111, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(112, 112, '2020-10-01', 10, 2020, 1, NULL, 35.00),
(113, 113, '2020-10-01', 10, 2020, 0, NULL, 50.00),
(114, 114, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(115, 115, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(116, 116, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(117, 117, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(118, 118, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(119, 119, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(120, 120, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(121, 121, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(122, 122, '2020-10-01', 10, 2020, 0, NULL, 60.00),
(123, 123, '2020-10-01', 10, 2020, 1, NULL, 55.00),
(124, 124, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(125, 125, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(126, 126, '2020-10-01', 10, 2020, 1, NULL, 50.00),
(127, 127, '2020-10-01', 10, 2020, 1, NULL, 90.00),
(128, 128, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(129, 129, '2020-10-01', 10, 2020, 0, NULL, 55.00),
(130, 130, '2020-10-01', 10, 2020, 0, NULL, 35.00),
(131, 131, '2020-10-01', 10, 2020, 1, NULL, 35.00),
(132, 132, '2020-10-01', 10, 2020, 1, NULL, 35.00),
(133, 133, '2020-10-01', 10, 2020, 1, NULL, 35.00),
(134, 134, '2020-10-01', 10, 2020, 1, NULL, 35.00),
(135, 135, '2020-10-01', 10, 2020, 1, NULL, 85.00),
(136, 136, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(137, 137, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(138, 138, '2020-10-01', 10, 2020, 0, NULL, 60.00),
(139, 139, '2020-10-01', 10, 2020, 1, NULL, 45.00),
(140, 140, '2020-10-01', 10, 2020, 1, NULL, 85.00),
(282, 1, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(283, 2, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(284, 3, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(285, 4, '2020-11-02', 11, 2020, 0, NULL, 55.00),
(286, 5, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(287, 6, '2020-11-02', 11, 2020, 0, NULL, 90.00),
(288, 7, '2020-11-02', 11, 2020, 0, NULL, 55.00),
(289, 8, '2020-11-02', 11, 2020, 0, NULL, 55.00),
(290, 9, '2020-11-02', 11, 2020, 0, NULL, 55.00),
(291, 10, '2020-11-02', 11, 2020, 0, NULL, 90.00),
(292, 11, '2020-11-02', 11, 2020, 0, NULL, 160.00),
(293, 12, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(294, 13, '2020-11-02', 11, 2020, 0, NULL, 100.00),
(295, 14, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(296, 15, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(297, 16, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(298, 17, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(299, 18, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(300, 19, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(301, 20, '2020-11-02', 11, 2020, 0, NULL, 90.00),
(302, 21, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(303, 22, '2020-11-02', 11, 2020, 0, NULL, 85.00),
(304, 23, '2020-11-02', 11, 2020, 0, NULL, 185.00),
(305, 24, '2020-11-02', 11, 2020, 0, NULL, 105.00),
(306, 25, '2020-11-02', 11, 2020, 0, NULL, 105.00),
(307, 26, '2020-11-02', 11, 2020, 0, NULL, 105.00),
(308, 27, '2020-11-02', 11, 2020, 0, NULL, 80.00),
(309, 28, '2020-11-02', 11, 2020, 0, NULL, 130.00),
(310, 29, '2020-11-02', 11, 2020, 0, NULL, 105.00),
(311, 30, '2020-11-02', 11, 2020, 0, NULL, 105.00),
(312, 31, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(313, 32, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(314, 33, '2020-11-02', 11, 2020, 0, NULL, 40.00),
(315, 34, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(316, 35, '2020-11-02', 11, 2020, 0, NULL, 35.00),
(317, 36, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(318, 37, '2020-11-02', 11, 2020, 0, NULL, 40.00),
(319, 38, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(320, 39, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(321, 40, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(322, 41, '2020-11-02', 11, 2020, 0, NULL, 105.00),
(323, 42, '2020-11-02', 11, 2020, 0, NULL, 90.00),
(324, 43, '2020-11-02', 11, 2020, 0, NULL, 90.00),
(325, 44, '2020-11-02', 11, 2020, 0, NULL, 90.00),
(326, 45, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(327, 46, '2020-11-02', 11, 2020, 0, NULL, 80.00),
(328, 47, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(329, 48, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(330, 49, '2020-11-02', 11, 2020, 0, NULL, 100.00),
(331, 51, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(332, 52, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(333, 53, '2020-11-02', 11, 2020, 0, NULL, 90.00),
(334, 54, '2020-11-02', 11, 2020, 0, NULL, 130.00),
(335, 55, '2020-11-02', 11, 2020, 0, NULL, 85.00),
(336, 56, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(337, 57, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(338, 58, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(339, 59, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(340, 60, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(341, 61, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(342, 62, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(343, 63, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(344, 64, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(345, 65, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(346, 66, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(347, 67, '2020-11-02', 11, 2020, 0, NULL, 95.00),
(348, 68, '2020-11-02', 11, 2020, 0, NULL, 95.00),
(349, 69, '2020-11-02', 11, 2020, 0, NULL, 95.00),
(350, 70, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(351, 71, '2020-11-02', 11, 2020, 0, NULL, 100.00),
(352, 72, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(353, 73, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(354, 74, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(355, 75, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(356, 76, '2020-11-02', 11, 2020, 0, NULL, 160.00),
(357, 77, '2020-11-02', 11, 2020, 0, NULL, 85.00),
(358, 78, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(359, 79, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(360, 80, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(361, 81, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(362, 82, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(363, 83, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(364, 84, '2020-11-02', 11, 2020, 0, NULL, 40.00),
(365, 85, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(366, 86, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(367, 87, '2020-11-02', 11, 2020, 0, NULL, 130.00),
(368, 88, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(369, 89, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(370, 90, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(371, 91, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(372, 92, '2020-11-02', 11, 2020, 0, NULL, 95.00),
(373, 93, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(374, 94, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(375, 95, '2020-11-02', 11, 2020, 0, NULL, 80.00),
(376, 96, '2020-11-02', 11, 2020, 0, NULL, 80.00),
(377, 97, '2020-11-02', 11, 2020, 0, NULL, 70.00),
(378, 98, '2020-11-02', 11, 2020, 0, NULL, 55.00),
(379, 99, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(380, 100, '2020-11-02', 11, 2020, 0, NULL, 55.00),
(381, 101, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(382, 102, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(383, 103, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(384, 104, '2020-11-02', 11, 2020, 0, NULL, 20.00),
(385, 105, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(386, 106, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(387, 107, '2020-11-02', 11, 2020, 0, NULL, 0.00),
(388, 108, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(389, 109, '2020-11-02', 11, 2020, 0, NULL, 80.00),
(390, 110, '2020-11-02', 11, 2020, 0, NULL, 90.00),
(391, 111, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(392, 112, '2020-11-02', 11, 2020, 0, NULL, 35.00),
(393, 113, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(394, 114, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(395, 115, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(396, 116, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(397, 117, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(398, 118, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(399, 119, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(400, 120, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(401, 121, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(402, 122, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(403, 123, '2020-11-02', 11, 2020, 0, NULL, 55.00),
(404, 124, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(405, 125, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(406, 126, '2020-11-02', 11, 2020, 0, NULL, 50.00),
(407, 127, '2020-11-02', 11, 2020, 0, NULL, 90.00),
(408, 128, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(409, 129, '2020-11-02', 11, 2020, 0, NULL, 55.00),
(410, 130, '2020-11-02', 11, 2020, 0, NULL, 35.00),
(411, 131, '2020-11-02', 11, 2020, 0, NULL, 35.00),
(412, 132, '2020-11-02', 11, 2020, 0, NULL, 35.00),
(413, 133, '2020-11-02', 11, 2020, 0, NULL, 35.00),
(414, 134, '2020-11-02', 11, 2020, 0, NULL, 35.00),
(415, 135, '2020-11-02', 11, 2020, 0, NULL, 85.00),
(416, 136, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(417, 137, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(418, 138, '2020-11-02', 11, 2020, 0, NULL, 60.00),
(419, 139, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(420, 140, '2020-11-02', 11, 2020, 0, NULL, 85.00),
(421, 141, '2020-11-02', 11, 2020, 0, NULL, 40.00),
(422, 142, '2020-11-02', 11, 2020, 0, NULL, 45.00),
(423, 143, '2020-11-02', 11, 2020, 0, NULL, 60.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(80) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `authKey` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `activate` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `authKey`, `accessToken`, `activate`) VALUES
(1, 'noelia', 'fs.1NfCM3grv2', 'academianoeliaperez@gmail.com', 'c4cd89d40a87b49ad3cf1a71ea316e5b8e4e2e82aeac13ddba57ce9f9e40fe64e32978cf3725fdeeedcdba455f581fead16209d851f6f865773d5baccc067665e628b34737e7a93cc29e88adae4b3aa39fcd546f559167d3eaa31c38404e8f36a9661572', '03d6f678079e86871df6851828fe591d3e055aa841722e122b34eaf019d004c24e4ef97727e142917e79d8831cc6fc60986452e7ae68ef59431b00b9c6b66a282c878c7ce16f137d1423cec623a9ac719ac2ad2a8f83fa050dda4048d3d5f49c6bfca089', 1),
(2, 'alberto', 'fspslPpolE7PY', 'albertopefer@hotmail.com', '38fc1f05b58de3c879d30c0ec179ebf0f2a45c5330138db2c7cbdd31a4488c08f671878c2812c5911816c1cf0c7c57a1fcc5b22c61a496646523a687768189e513c5ff5802e2cb47f04acc8027d61bd0ad5a98ccfb55ea7e4a8d3e2585170b3116b5848b', 'fcda675f038056bda53316afeb36dd6505ffc53951829ee66ae015666bb6ad9a12f5c1f9a167159da289f27e9f975d3efc5e03f85257dddf3914643742edb3d74ee7e5b4bdbd2b6935ab1b7eba85f61ec7278b1d727411ad52e4f4c2eb1cb8c1b94f43b2', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agrupan`
--
ALTER TABLE `agrupan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alumno` (`alumno`,`alumno_grupo`);

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `alumnos_centros` (`centro`),
  ADD KEY `dni` (`dni`) USING BTREE;

--
-- Indices de la tabla `asignaturas`
--
ALTER TABLE `asignaturas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `centros`
--
ALTER TABLE `centros`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `clases`
--
ALTER TABLE `clases`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `curso` (`curso`,`asignatura`),
  ADD KEY `clases_asignatura` (`asignatura`);

--
-- Indices de la tabla `cuentas_bancarias`
--
ALTER TABLE `cuentas_bancarias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cuentasbancarias_alumnos` (`alumno`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `imparten`
--
ALTER TABLE `imparten`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profesor` (`profesor`,`asignatura`),
  ADD KEY `imparten_asignaturas` (`asignatura`);

--
-- Indices de la tabla `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `material_alumno` (`alumno`);

--
-- Indices de la tabla `matriculas`
--
ALTER TABLE `matriculas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alumno` (`alumno`,`clase`,`alta`),
  ADD KEY `matriculan_clases` (`clase`);

--
-- Indices de la tabla `profesores`
--
ALTER TABLE `profesores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni` (`dni`);

--
-- Indices de la tabla `recibos`
--
ALTER TABLE `recibos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recibos_matriculas` (`matricula`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`,`password`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agrupan`
--
ALTER TABLE `agrupan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT de la tabla `asignaturas`
--
ALTER TABLE `asignaturas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `centros`
--
ALTER TABLE `centros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `clases`
--
ALTER TABLE `clases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `cuentas_bancarias`
--
ALTER TABLE `cuentas_bancarias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `imparten`
--
ALTER TABLE `imparten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `material`
--
ALTER TABLE `material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT de la tabla `matriculas`
--
ALTER TABLE `matriculas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT de la tabla `profesores`
--
ALTER TABLE `profesores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `recibos`
--
ALTER TABLE `recibos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=424;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `agrupan`
--
ALTER TABLE `agrupan`
  ADD CONSTRAINT `agrupan_alumnos` FOREIGN KEY (`alumno`) REFERENCES `alumnos` (`id`);

--
-- Filtros para la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD CONSTRAINT `alumnos_centros` FOREIGN KEY (`centro`) REFERENCES `centros` (`id`);

--
-- Filtros para la tabla `clases`
--
ALTER TABLE `clases`
  ADD CONSTRAINT `clases_asignatura` FOREIGN KEY (`asignatura`) REFERENCES `asignaturas` (`id`),
  ADD CONSTRAINT `clases_curso` FOREIGN KEY (`curso`) REFERENCES `cursos` (`id`);

--
-- Filtros para la tabla `cuentas_bancarias`
--
ALTER TABLE `cuentas_bancarias`
  ADD CONSTRAINT `cuentasbancarias_alumnos` FOREIGN KEY (`alumno`) REFERENCES `alumnos` (`id`);

--
-- Filtros para la tabla `imparten`
--
ALTER TABLE `imparten`
  ADD CONSTRAINT `imparten_asignaturas` FOREIGN KEY (`asignatura`) REFERENCES `asignaturas` (`id`),
  ADD CONSTRAINT `imparten_profesores` FOREIGN KEY (`profesor`) REFERENCES `profesores` (`id`);

--
-- Filtros para la tabla `material`
--
ALTER TABLE `material`
  ADD CONSTRAINT `material_alumno` FOREIGN KEY (`alumno`) REFERENCES `alumnos` (`id`);

--
-- Filtros para la tabla `matriculas`
--
ALTER TABLE `matriculas`
  ADD CONSTRAINT `matriculan_alumnos` FOREIGN KEY (`alumno`) REFERENCES `alumnos` (`id`),
  ADD CONSTRAINT `matriculan_clases` FOREIGN KEY (`clase`) REFERENCES `clases` (`id`);

--
-- Filtros para la tabla `recibos`
--
ALTER TABLE `recibos`
  ADD CONSTRAINT `recibos_matriculas` FOREIGN KEY (`matricula`) REFERENCES `matriculas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
